﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Audio;

namespace ShowerDefence
{
    class Button
    {

        //attributes
        int x = 0;
        int y = 0;
        int width;
        int height;
        Texture2D invis;
        Point pt;
        Rectangle b;
        Rectangle mouse;
        bool pressed;
        bool isActive;
        bool dr; // for tower menus only, drawing 
        Color col;
        

        //constructor 
        public Button(int xVal, int yVal, int w, int h, Texture2D sprite)
        {
            x = xVal;
            y = yVal;
            width = w;
            height = h;
            invis = sprite;
            pt = new Point(x, y);
            b = new Rectangle(x, y, width, height);
            pressed = false;
            isActive = false;
            dr = false;
            col = Color.Green;

        }
        //properties

        public bool IsActive
        {
            get { return isActive; }
            set { isActive = value; }

        }
        public Color Col
        {
            get { return col; }
            set { col = value; }
        }

        public bool Pressed
        {
            get { return pressed; }
            set { pressed = value; }
        }

        public bool Dr
        {
            get { return dr; }
            set { dr = value; }
        }

        //update method
        public virtual void Update(MouseState mState, MouseState mStatePrev)
        {
            mouse = new Rectangle(mState.X, mState.Y, 1, 1);

            if (isActive == true && (mState.X >= x && mState.X <= x + width) && (mState.Y >= y && mState.Y <= y + height) && mState.LeftButton == ButtonState.Pressed && mStatePrev.LeftButton == ButtonState.Released)
            {
                pressed = true;
            }
        }

        //draw the button 
        public virtual void Draw(SpriteBatch sb)
        {

            if (isActive == true)
            {
                if (mouse.Intersects(b) == true)
                {
                    sb.Draw(invis, mouse, Color.White);
                    sb.Draw(invis, b, col);
                }
                else
                {
                    sb.Draw(invis, mouse, Color.White);
                    sb.Draw(invis, b, Color.Transparent);
                }

            }
            
        }
    }
}
