﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Audio;

// Weapons - parent class for all towers and usables to kill enemies
namespace ShowerDefence
{
    class Weapon
    {
        // attributes
        int cost, damage, range, sellPrice;
        double atkSpeed;
        bool airShot;
        Texture2D image, bulletImage;
        Rectangle rect;
        Color clr;
        int upgradeLvl;
        bool paused = false; // is the game paused?
        int shotTimer = 0;
        bool firing;
        bool sold;
        bool regen = false;
        Enemy target;

        // constructor
        public Weapon(int cst, int dmg, int rng, int sell, double atkSp, bool airSh, Texture2D pic, Rectangle r, Color c, Texture2D bulImg)
        {
            cost = cst;
            damage = dmg;
            range = rng;
            sellPrice = sell;
            atkSpeed = atkSp;
            airShot = airSh;
            image = pic;
            rect = r;
            clr = c;
            upgradeLvl = 0;
            bulletImage = bulImg;
            firing = false;
        }

        // constructor for hairbrushes
        public Weapon(int cst, int dmg, int rng, int sell, double atkSp, bool airSh, Texture2D pic, Rectangle r, Color c)
        {
            cost = cst;
            damage = dmg;
            range = rng;
            sellPrice = sell;
            atkSpeed = atkSp;
            airShot = airSh;
            image = pic;
            rect = r;
            clr = c;
            upgradeLvl = 0;
            firing = false;
        }

        // properties
        public int Cost
        {
            get { return cost; }
        }
        public int Damage
        {
            get { return damage; }
            set { damage = value; }
        }
        public int Range
        {
            get { return range; }
            set { range = value; }
        }
        public int SellPrice
        {
            get { return sellPrice; }
            set { sellPrice = value; }
        }
        public bool Firing
        {
            get { return firing; }
            set { firing = value; }
        }
        public double AtkSpeed
        {
            get { return atkSpeed; }
            set { atkSpeed = value; }
        }
        public Texture2D Image
        {
            get { return image; }
        }
        public Rectangle Rect
        {
            get { return rect; }
            set { rect = value; }
        }
        public int RectX
        {
            get { return rect.X; }
            set { rect.X = value; }
        }
        public int RectY
        {
            get { return rect.Y; }
            set { rect.Y = value; }
        }
        public Color Clr
        {
            get { return clr; }
            set { clr = value; }
        }
        public int UpgrdLvl
        {
            get { return upgradeLvl; }
            set { upgradeLvl = value; }
        }
        public bool Paused
        {
            get { return paused; }
            set { paused = value; }
        }
        public bool AirShot
        {
            get { return airShot;  }
            set { airShot = value; }
        }
        public bool Sold
        {
            get { return sold; }
            set { sold = value; }
        }
        public bool Regen
        {
            get { return regen; }
            set { regen = value; }
        }
        public Texture2D BulletImage
        {
            get { return bulletImage; }
            set { bulletImage = value;}
        }

        public int ShotTimer
        {
            get { return shotTimer; }
            set { shotTimer = value; }
        }
        public Enemy Target
        {
            get { return target; }
            set { target = value; }
        }

        // Shoot Method
        Rectangle bulletPos = new Rectangle();
        bool newBullet = true;
        public void Shoot(Enemy bugObj, SpriteBatch spriteBatch, Texture2D bullet, SoundEffect shoot)
        {
            if (!paused)
            {
                if (bugObj.Rect.Center.X - Rect.Center.X != 0 && bugObj.Rect.Center.Y - Rect.Center.Y != 0)
                {
                    if (bugObj.IsActive == true)
                    {

                        int vectXStore = (int)bugObj.Position.X;
                        int vectYStore = (int)bugObj.Position.Y;
                        bugObj.Rect = new Rectangle(vectXStore, vectYStore, bugObj.Texture.Width, bugObj.Texture.Height);
                    }

                    Point distance = Rect.Center - bugObj.Rect.Center;
                    double yDif = distance.Y;
                    double xDif = distance.X;


                    if (yDif < 0)
                        yDif = -yDif;
                    if (xDif < 0)
                        xDif = -xDif;

                    int aproxXDif = (int)xDif;
                    int aproxYDif = (int)yDif;


                    if (Math.Sqrt((xDif * xDif) + (yDif * yDif)) <= (13.5 + (27 * Range)))
                    {


                        if (newBullet == true)
                        {
                            bulletPos = new Rectangle(Rect.Center.X, Rect.Center.Y, bullet.Width, bullet.Height);
                            try
                            {
                                SoundEffectInstance soundInstance = shoot.CreateInstance();
                                soundInstance.Volume = 0.1f;
                                soundInstance.Play();
                            }
                            catch { }
                            newBullet = false;
                        }
                        else
                        {
                            if (bugObj.Rect.X > bulletPos.X && bugObj.Rect.Y == bulletPos.Y)
                                bulletPos = new Rectangle(bulletPos.X + aproxXDif / 20, bulletPos.Y, bullet.Width, bullet.Height);

                            if (bugObj.Rect.X == bulletPos.X && bugObj.Rect.Y > bulletPos.Y)
                                bulletPos = new Rectangle(bulletPos.X, bulletPos.Y + aproxYDif / 20, bullet.Width, bullet.Height);

                            if (bugObj.Rect.X > bulletPos.X && bugObj.Rect.Y > bulletPos.Y)
                                bulletPos = new Rectangle(bulletPos.X + aproxXDif / 20, bulletPos.Y + aproxYDif / 20, bullet.Width, bullet.Height);

                            if (bugObj.Rect.X < bulletPos.X && bugObj.Rect.Y == bulletPos.Y)
                                bulletPos = new Rectangle(bulletPos.X - aproxXDif / 20, bulletPos.Y, bullet.Width, bullet.Height);

                            if (bugObj.Rect.X == bulletPos.X && bugObj.Rect.Y < bulletPos.Y)
                                bulletPos = new Rectangle(bulletPos.X, bulletPos.Y - aproxYDif / 20, bullet.Width, bullet.Height);

                            if (bugObj.Rect.X < bulletPos.X && bugObj.Rect.Y < bulletPos.Y)
                                bulletPos = new Rectangle(bulletPos.X - aproxXDif / 20, bulletPos.Y - aproxYDif / 20, bullet.Width, bullet.Height);

                            if (bugObj.Rect.X < bulletPos.X && bugObj.Rect.Y > bulletPos.Y)
                                bulletPos = new Rectangle(bulletPos.X - aproxXDif / 20, bulletPos.Y + aproxYDif / 20, bullet.Width, bullet.Height);

                            if (bugObj.Rect.X > bulletPos.X && bugObj.Rect.Y < bulletPos.Y)
                                bulletPos = new Rectangle(bulletPos.X + aproxXDif / 20, bulletPos.Y - aproxYDif / 20, bullet.Width, bullet.Height);
                        }
                        spriteBatch.Draw(bullet, bulletPos, Color.White);

                        if (bulletPos.Intersects(bugObj.Rect))
                        {
                            bugObj.Health = bugObj.Health - Damage;
                            newBullet = true;
                            firing = false;
                            if (bugObj.Health <= 0)
                            {
                                bugObj.Rect = new Rectangle(-100000, -100000, 1, 1);
                                bugObj.Die();
                            }
                        }
                    }
                }
                
            }
        }

        public async void ShotsPerSecond()
        {
            for (int j = 0; j < 30;)
            {
                if (!paused) // while spawning the enemies in delay, check to see if the game paused 50 times during the two second delay
                {
                    await Task.Delay(Convert.ToInt32(atkSpeed)); // if not than continue the delay to spawn the next enemy
                    j++;
                }
            }
            this.Firing = true;
            this.regen = false;
            return;
        }
        // Sell Method
        public void Sell()
        {

        }

        // Place Method
        public void Place(Weapon tower, MouseState mState)
        {
            // if it's a lotion tower or pill shooter, follow these guidelines
            if(tower is Lotion || tower is PillShoot)
            {
                // checking to see if the mouse intersects with path 1
                if ((mState.X) / 27 > 5 && mState.X / 27 < 8 && (mState.Y) / 27 <= 18)
                {
                    tower.Clr = Color.Red;
                }
                // path 2
                else if (mState.Y / 27 > 16 && mState.Y / 27 < 19 && (mState.X / 27 >= 7 && mState.X / 27 <= 22))
                {
                    tower.Clr = Color.Red;
                }
                // path 3
                else if (mState.X / 27 > 21 && mState.X / 27 < 24 && (mState.Y / 27 >= 12 && mState.Y / 27 <= 17))
                {
                    tower.Clr = Color.Red;
                }
                // path 4
                else if (mState.Y / 27 > 11 && mState.Y / 27 < 14 && (mState.X / 27 >= 9 && mState.X / 27 <= 22))
                {
                    tower.Clr = Color.Red;
                }
                // path 5
                else if (mState.X / 27 > 8 && mState.X / 27 < 11 && (mState.Y / 27 >= 3 && mState.Y / 27 <= 12))
                {
                    tower.Clr = Color.Red;
                }
                // path 6
                else if (mState.Y / 27 > 2 && mState.Y / 27 < 5 && (mState.X / 27 >= 9 && mState.X / 27 <= 15))
                {
                    tower.Clr = Color.Red;
                }
                // path 7
                else if (mState.X / 27 > 13 && mState.X / 27 < 16 && (mState.Y / 27 >= 4 && mState.Y / 27 <= 10))
                {
                    tower.Clr = Color.Red;
                }
                // path 8
                else if (mState.Y / 27 > 8 && mState.Y / 27 < 11 && (mState.X / 27 >= 15 && mState.X / 27 <= 18))
                {
                    tower.Clr = Color.Red;
                }
                // path 9
                else if (mState.X / 27 > 16 && mState.X / 27 < 19 && (mState.Y / 27 >= 3 && mState.Y / 27 <= 8))
                {
                    tower.Clr = Color.Red;
                }
                // path 10
                else if (mState.Y / 27 > 2 && mState.Y / 27 < 5 && (mState.X / 27 >= 19 && mState.X / 27 <= 22))
                {
                    tower.Clr = Color.Red;
                }
                // path 11
                else if (mState.X / 27 > 21 && mState.X / 27 < 24 && (mState.Y / 27 >= 4 && mState.Y / 27 <= 9))
                {
                    tower.Clr = Color.Red;
                }
                // path 12
                else if (mState.Y / 27 > 8 && mState.Y / 27 < 11 && (mState.X / 27 >= 22 && mState.X / 27 <= 27))
                {
                    tower.Clr = Color.Red;
                }
                // shower
                else if ((mState.Y / 27 >= 5 && mState.Y / 27 <= 16) && (mState.X / 27 >= 27 && mState.X / 27 <= 30))
                {
                    tower.Clr = Color.Red;
                }
                // pause/end button
                else if ((mState.Y / 27 >= 0 && mState.Y / 27 <= 3) && (mState.X / 27 >= 0 && mState.X / 27 <= 6))
                {
                    tower.Clr = Color.Red;
                }
                // start button
                else if ((mState.Y / 27 >= 0 && mState.Y / 27 <= 3) && (mState.X / 27 >= 25 && mState.X / 27 <= 31))
                {
                    tower.Clr = Color.Red;
                }
                // offscreen and off the grid
                else if ((mState.Y / 27 > 19 || mState.Y < 0) || (mState.X < 0 || mState.X / 27 > 30))
                {
                    tower.Clr = Color.Red;
                }
                // make it green if it's anywhere else
                else
                {
                    tower.Clr = Color.LightGreen;
                }
            }

            // if it's a hairbrush, follow these guidelines
            if(tower is Hairbrush)
            {
                Hairbrush h = (Hairbrush)tower;
                // if it's on path 1, make it green
                if(mState.X / 27 == 7 && mState.Y / 27 >= 0 && mState.Y / 27 <= 18)
                {
                    tower.Clr = Color.LightGreen;
                    h.Col = Color.LightGreen;
                }
                // path 2
                else if(mState.Y / 27 == 18 && mState.X / 27 >= 7 && mState.X / 27 <= 23)
                {
                    tower.Clr = Color.LightGreen;
                    h.Col = Color.LightGreen;
                }
                // path 3
                else if (mState.X / 27 == 23 && mState.Y / 27 <= 18 && mState.Y / 27 >= 13)
                {
                    tower.Clr = Color.LightGreen;
                    h.Col = Color.LightGreen;
                }
                // path 4
                else if (mState.Y / 27 == 13 && mState.X / 27 >= 10 && mState.X / 27 <= 23)
                {
                    tower.Clr = Color.LightGreen;
                    h.Col = Color.LightGreen;
                }
                // path 5
                else if (mState.X / 27 == 10 && mState.Y / 27 <= 13 && mState.Y / 27 >= 4)
                {
                    tower.Clr = Color.LightGreen;
                    h.Col = Color.LightGreen;
                }
                // path 6
                else if (mState.Y / 27 > 3 && mState.Y / 27 < 5 && (mState.X / 27 >= 9 && mState.X / 27 <= 15))
                {
                    tower.Clr = Color.LightGreen;
                    h.Col = Color.LightGreen;
                }
                // path 7
                else if (mState.X / 27 > 14 && mState.X / 27 < 16 && (mState.Y / 27 >= 4 && mState.Y / 27 <= 10))
                {
                    tower.Clr = Color.LightGreen;
                    h.Col = Color.LightGreen;
                }
                // path 8
                else if (mState.Y / 27 > 9 && mState.Y / 27 < 11 && (mState.X / 27 >= 15 && mState.X / 27 <= 18))
                {
                    tower.Clr = Color.LightGreen;
                    h.Col = Color.LightGreen;
                }
                // path 9
                else if (mState.X / 27 > 17 && mState.X / 27 < 19 && (mState.Y / 27 >= 4 && mState.Y / 27 <= 9))
                {
                    tower.Clr = Color.LightGreen;
                    h.Col = Color.LightGreen;
                }
                // path 10
                else if (mState.Y / 27 > 3 && mState.Y / 27 < 5 && (mState.X / 27 >= 19 && mState.X / 27 <= 22))
                {
                    tower.Clr = Color.LightGreen;
                    h.Col = Color.LightGreen;
                }
                // path 11
                else if (mState.X / 27 > 22 && mState.X / 27 < 24 && (mState.Y / 27 >= 4 && mState.Y / 27 <= 9))
                {
                    tower.Clr = Color.LightGreen;
                    h.Col = Color.LightGreen;
                }
                // path 12
                else if (mState.Y / 27 > 9 && mState.Y / 27 < 11 && (mState.X / 27 >= 23 && mState.X / 27 <= 27))
                {
                    tower.Clr = Color.LightGreen;
                    h.Col = Color.LightGreen;
                }
                // otherwise make it red if it's not on any of the paths
                else
                {
                    tower.Clr = Color.Red;
                    h.Col = Color.Red;
                }
            }
            
        }

        //Upgrade Method
        public virtual void Upgrade()
        {
            upgradeLvl++;
        }
    }
}
