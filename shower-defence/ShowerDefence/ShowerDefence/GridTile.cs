﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Audio;

namespace ShowerDefence
{
    public class GridTile
    {
        Texture2D image;
        Rectangle position;
        bool canPlace;
        bool isTower;
        bool brushPlace;

        public GridTile(Texture2D texture, Rectangle posRect, bool place, bool tower)
        {
            image = texture;
            position = posRect;
            canPlace = place;
            isTower = tower;
        }

        public bool HairbrushPlace
        {
            get { return brushPlace;  }
            set { brushPlace = value; }
        }

        public Texture2D Image
        {
            get { return image;  }
            set { image = value; }
        }
        public Rectangle Pos
        {
            get { return position;  }
            set { position = value; }
        }
        public int PosX
        {
            get { return position.X; }
            set { position.X = value; }
        }
        public int PosY
        {
            get { return position.Y; }
            set { position.Y = value; }
        }
        public bool Placeable
        {
            get { return canPlace;  }
            set { canPlace = value; }
        }
        public bool TowerOn
        {
            get { return isTower;  }
            set { isTower = value; }
        }
    }
}
