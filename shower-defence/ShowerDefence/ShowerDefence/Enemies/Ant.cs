﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace ShowerDefence
{
    public class Ant : Enemy
    {
        //attributes 
        int frame; //current frame number
        // constructor
        public Ant(int hlth, int spd, int dmg, int rwrd, bool fly, Texture2D t, Vector2 pos, Point size, int frames, int msPerFrame, Point cf, int f, Player plyr):base(hlth, spd, dmg, rwrd,fly, t, pos, size, frames, msPerFrame, cf, plyr)  // constructor
        {
            // assigned to the enemy class
            currentFrame = cf;
            frame = f;
        }

        //switch the frame being displayed for the ant
        public override void Update(GameTime gameTime)
        {
            //update elapsed time 
            timeSinceLastFrame += gameTime.ElapsedGameTime.Milliseconds;

            //time to change frame
            if (timeSinceLastFrame > millisecondsPerFrame)
            {
                timeSinceLastFrame = 0;
                frame++;
            }

            //loop frames
            if (frame > numFrames)
            {
                frame = 1; //restart
            }

            switch (frame)
            {
                case 1:
                    currentFrame.X = 0;
                    currentFrame.Y = 0;
                    break;
                case 2:
                    currentFrame.X = 9;
                    currentFrame.Y = 0;
                    break;
                case 3:
                    currentFrame.X = 18;
                    currentFrame.Y = 0;
                    break;
                case 4:
                    currentFrame.X = 27;
                    currentFrame.Y = 0;
                    break;
                
            }

            base.Update(gameTime);
        }
    }
}
