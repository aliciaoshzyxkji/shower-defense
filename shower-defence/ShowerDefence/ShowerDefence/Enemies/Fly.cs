﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace ShowerDefence
{
    public class Fly : Enemy
    {
        // constructor
        public Fly(int hlth, int spd, int dmg, int rwrd, bool fly, Texture2D t, Vector2 pos, Point size, int frames, int msPerFrame, Point cf, Player plyr) : base(hlth, spd, dmg, rwrd, fly, t, pos, size, frames, msPerFrame, cf, plyr) // constructor
        {
            // assigned to the enemy class
        }
    }
}