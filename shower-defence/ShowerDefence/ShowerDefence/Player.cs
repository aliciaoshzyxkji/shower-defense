﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

// Player Class

namespace ShowerDefence
{
    public class Player
    {
        // attributes
        int playerHealth;
        int bubbles;
        int enemiesLeft;

        // properties
        public int PlayerHealth
        {
            get { return playerHealth; }
            set { playerHealth = value; }
        }

        public int Bubbles
        {
            get { return bubbles; }
            set { bubbles = value; }
        }

        public int EnemiesLeft
        {
            get { return enemiesLeft; }
            set { enemiesLeft = value; }
        }

        public Player(int ph, int bub) // constructor
        {
            playerHealth = ph;
            bubbles = bub;
        }
    }
}
