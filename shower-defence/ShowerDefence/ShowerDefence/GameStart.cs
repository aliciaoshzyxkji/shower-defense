﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework;

namespace ShowerDefence
{
    class GameStart : Button
    {
        int x = 0;
        int y = 0;
        int width;
        int height;
        Texture2D invis;
        Point pt;
        Rectangle b;
        Rectangle mouse;
        Game1 obj = new Game1();

        public GameStart(int xVal, int yVal, int w, int h, Texture2D sprite) : base(xVal, yVal, w, h, sprite)
        {
            x = xVal;
            y = yVal;
            width = w;
            height = h;
            invis = sprite;
            pt = new Point(x, y);
            b = new Rectangle(x, y, width, height);
        }

        //update method override 
        public override void Update(MouseState mState, MouseState mStatePrev)
        {
            mouse = new Rectangle(mState.X, mState.Y, 1, 1);


            if (mouse.Intersects(b) || mState.RightButton == ButtonState.Pressed && mStatePrev.RightButton == ButtonState.Released)
            {
                //obj.newGame
            }
            base.Update(mState, mStatePrev);
        }
    }
}
