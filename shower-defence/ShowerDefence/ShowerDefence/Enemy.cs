﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

// Enemy Class - the enemies within the game
namespace ShowerDefence
{
    
    public class Enemy
    {
        enum State { Up, Down, Left, Right } 

        // attributes
        int health, speed, damage, reward;
        bool isFlying;
        bool isActive = true;
        Texture2D texture; //sprite texture 
        protected int frame; //current frame number 
        protected Point frameSize; //width and height of image 
        protected int numFrames; //number of frames in the spritesheet
        protected int timeSinceLastFrame; //elapsed time since frame was drawn 
        protected int millisecondsPerFrame; //millsec to display a frame 
        protected Point currentFrame; //location of current frame on spritesheet 
        Vector2 position;
        int waypoint = 1;
        int tempWaypoint = 1;
        Player player; // the current player
        Rectangle rect;
        bool paused = false; // is the game paused?

        //state for animation
        State direction;

        public int Health // property
        {
            get { return health; }
            set { health = value; }
        }

        public int Speed // property
        {
            get { return speed; }
            set { speed = value; }
        }

        public int Damage // property
        {
            get { return damage; }
            set { damage = value; }
        }

        public int Reward // property
        {
            get { return reward; }
            set { reward = value; }
        }

        public bool IsFlying // property
        {
            get { return isFlying; }
            set { isFlying = value; }
        }
        public bool IsActive // property
        {
            get { return isActive; }
            set { isActive = value; }
        }
        public Texture2D Texture // property
        {
            get { return texture; }
            set { texture = value; }
        }

        public Vector2 Position // property
        {
            get { return position; }
            set { position = value; }
        }
        public int posX // property
        {
            get { return (int)position.X; }
            set { position.X = value; }
        }
        public int posY // property
        {
            get { return (int)position.Y; }
            set { position.Y = value; }
        }
        public int RectX
        {
            get { return rect.X; }
            set { rect.X = value; }
        }
        public int RectY
        {
            get { return rect.Y; }
            set { rect.Y = value; }
        }
        public int Waypoint // property
        {
            get { return waypoint; }
            set { waypoint = value; }
        }
        public int TempWaypoint // property
        {
            get { return tempWaypoint; }
            set { tempWaypoint = value; }
        }
        public Point FrameSize //property
        {
            get { return frameSize; }
        }
        public Rectangle Rect //property
        {
            get { return rect; }
            set { rect = value; }
        }
        public bool Paused //property
        {
            get { return paused; }
            set { paused = value; }
        }

        // constructor
        public Enemy(int hlth, int spd, int dmg, int rwrd, bool fly, Texture2D t, Vector2 pos, Point size, int frames, int msPerFrame, Point cf, Player plyr)
        {
            health = hlth;
            speed = spd;
            damage = dmg;
            reward = rwrd;
            isFlying = fly;
            texture = t;
            position = pos;
            frameSize = size;
            numFrames = frames;
            millisecondsPerFrame = msPerFrame;
            cf = currentFrame;
            currentFrame.X = cf.X;
            currentFrame.Y = cf.Y;
            player = plyr;
        }
        public virtual void Update(GameTime gameTime)
        {
            if (isActive)
            {
                //update elapsed time 
                timeSinceLastFrame += gameTime.ElapsedGameTime.Milliseconds;

                //time to change frame
                if (timeSinceLastFrame > millisecondsPerFrame)
                {
                    timeSinceLastFrame = 0;
                    frame++;
                }

                //out of frames, return to start
                if (frame >= numFrames)
                {
                    frame = 0; //restart
                }
            }
        }
        public virtual void Draw(SpriteBatch obj, GameTime gameTime) // call the draw method
        {
            if (isActive == true)
            {
                rect = new Rectangle(currentFrame.X, currentFrame.Y, frameSize.X, frameSize.Y);

                if (direction == State.Up)
                {
                    obj.Draw(this.Texture, position, rect, Color.White, 0, new Vector2(this.frameSize.X / 2, this.frameSize.Y / 2), 1, SpriteEffects.None, 0);
                }
                else if (direction == State.Down)
                {
                    obj.Draw(this.Texture, position, rect, Color.White, 0, new Vector2(this.frameSize.X / 2, this.frameSize.Y / 2), 1, SpriteEffects.FlipVertically, 0);
                }
                else if (direction == State.Left)
                {
                    obj.Draw(this.Texture, position, rect, Color.White, -1.57f, new Vector2(this.frameSize.X / 2, this.frameSize.Y / 2), 1, SpriteEffects.None, 0);
                }
                else if (direction == State.Right)
                {
                    obj.Draw(this.Texture, position, rect, Color.White, 4.71f, new Vector2(this.frameSize.X / 2, this.frameSize.Y / 2), 1, SpriteEffects.FlipVertically, 0);
                }
            }
        }

        // methods below are not necessarily public, void, or with 0 parameters

        // Move method
        public async void Move(GridTile[,] tileArray) // moving along the path
        {
            if (isActive == true)
            {
                while (waypoint == 1) // movement to the corner of path 1
                {
                    if (!paused && isActive)
                    {
                        direction = State.Down;
                        await Task.Delay(500 / speed); // delay the movement of the sprite 
                        this.Position = new Vector2(this.Position.X + 0, this.Position.Y + 1); // move the sprite
                        if (Position.Y >= tileArray[7, 18].PosY + 13.5f) // once the enemy gets to the corner of that path
                        {
                            direction = State.Right;
                            waypoint = 2; // go to the next corner (waypoint)
                        }
                    }
                }

                while (waypoint == 2) // movement to the corner of path 2
                {
                    if (!paused && isActive)
                    {
                        await Task.Delay(500 / speed); // delay the movement of the sprite 
                        this.Position = new Vector2(this.Position.X + 1, this.Position.Y + 0); // move the sprite
                        if (Position.X >= tileArray[23, 18].PosX + 13.5f) // once the enemy gets to the corner of that path
                        {
                            direction = State.Up;
                            waypoint = 3; // go to the next corner (waypoint)
                        }
                    }
                }

                while (waypoint == 3) // movement to the corner of path 3
                {
                    if (!paused && isActive)
                    {
                        await Task.Delay(500 / speed); // delay the movement of the sprite 
                        this.Position = new Vector2(this.Position.X + 0, this.Position.Y - 1); // move the sprite
                        if (Position.Y <= tileArray[23, 13].PosY + 13.5f) // once the enemy gets to the corner of that path
                        {
                            direction = State.Left;
                            waypoint = 4; // go to the next corner (waypoint)
                        }
                    }
                }

                while (waypoint == 4) // movement to the corner of path 4
                {
                    if (!paused && isActive)
                    {
                        await Task.Delay(500 / speed); // delay the movement of the sprite 
                        this.Position = new Vector2(this.Position.X - 1, this.Position.Y + 0); // move the sprite
                        if (Position.X <= tileArray[10, 13].PosX + 13.5f) // once the enemy gets to the corner of that path
                        {
                            direction = State.Up;
                            waypoint = 5; // go to the next corner (waypoint)
                        }
                    }
                }

                while (waypoint == 5) // movement to the corner of path 5
                {
                    if (!paused && isActive)
                    {
                        await Task.Delay(500 / speed); // delay the movement of the sprite 
                        this.Position = new Vector2(this.Position.X + 0, this.Position.Y - 1); // move the sprite
                        if (Position.Y <= tileArray[10, 4].PosY + 13.5f) // once the enemy gets to the corner of that path
                        {
                            direction = State.Right;
                            waypoint = 6; // go to the next corner (waypoint)
                        }
                    }
                }

                while (waypoint == 6) // movement to the corner of path 6
                {
                    if (!paused && isActive)
                    {
                        await Task.Delay(500 / speed); // delay the movement of the sprite 
                        this.Position = new Vector2(this.Position.X + 1, this.Position.Y + 0); // move the sprite
                        if (Position.X >= tileArray[15, 4].PosX + 13.5f) // once the enemy gets to the corner of that path
                        {
                            direction = State.Down;
                            waypoint = 7; // go to the next corner (waypoint)
                        }
                    }
                }

                while (waypoint == 7) // movement to the corner of path 7
                {
                    if (!paused && isActive)
                    {
                        await Task.Delay(500 / speed); // delay the movement of the sprite 
                        this.Position = new Vector2(this.Position.X + 0, this.Position.Y + 1); // move the sprite
                        if (Position.Y >= tileArray[15, 10].PosY + 13.5f) // once the enemy gets to the corner of that path
                        {
                            direction = State.Right;
                            waypoint = 8; // go to the next corner (waypoint)
                        }
                    }
                }

                while (waypoint == 8) // movement to the corner of path 8
                {
                    if (!paused && isActive)
                    {
                        await Task.Delay(500 / speed); // delay the movement of the sprite 
                        this.Position = new Vector2(this.Position.X + 1, this.Position.Y + 0); // move the sprite
                        if (Position.X >= tileArray[18, 10].PosX + 13.5f) // once the enemy gets to the corner of that path
                        {
                            direction = State.Up;
                            waypoint = 9; // go to the next corner (waypoint)
                        }
                    }
                }

                while (waypoint == 9) // movement to the corner of path 9
                {
                    if (!paused && isActive)
                    {
                        await Task.Delay(500 / speed); // delay the movement of the sprite 
                        this.Position = new Vector2(this.Position.X + 0, this.Position.Y - 1); // move the sprite
                        if (Position.Y <= tileArray[18, 4].PosY + 13.5f) // once the enemy gets to the corner of that path
                        {
                            direction = State.Right;
                            waypoint = 10; // go to the next corner (waypoint)
                        }
                    }
                }

                while (waypoint == 10) // movement to the corner of path 10
                {
                    if (!paused && isActive)
                    {
                        await Task.Delay(500 / speed); // delay the movement of the sprite 
                        this.Position = new Vector2(this.Position.X + 1, this.Position.Y + 0); // move the sprite
                        if (Position.X >= tileArray[23, 4].PosX + 13.5f) // once the enemy gets to the corner of that path
                        {
                            direction = State.Down;
                            waypoint = 11; // go to the next corner (waypoint)
                        }
                    }
                }

                while (waypoint == 11) // movement to the corner of path 11
                {
                    if (!paused && isActive)
                    {
                        await Task.Delay(500 / speed); // delay the movement of the sprite 
                        this.Position = new Vector2(this.Position.X + 0, this.Position.Y + 1); // move the sprite
                        if (Position.Y >= tileArray[23, 10].PosY + 13.5f) // once the enemy gets to the corner of that path
                        {
                            direction = State.Right;
                            waypoint = 12; // go to the next corner (waypoint)
                        }
                    }
                }

                while (waypoint == 12) // movement to the corner of path 12
                {
                    if (!paused && isActive)
                    {
                        await Task.Delay(500 / speed); // delay the movement of the sprite 
                        this.Position = new Vector2(this.Position.X + 1, this.Position.Y + 0); // move the sprite
                        if (Position.X >= tileArray[27, 10].PosX + 13.5f) // once the enemy gets to the corner of that path
                        {
                            Attack();
                            return;
                        }
                    }
                }
            }
            else
            {
                return;
            }
        }

        public bool InRange(Rectangle turretRect, int turretRange)
        {
            if (IsActive == true)
            {

                int vectXStore = (int)Position.X;
                int vectYStore = (int)Position.Y;
                Rect = new Rectangle(vectXStore, vectYStore, Texture.Width, Texture.Height);
            }
            Point distance = turretRect.Center - Rect.Center;
            double yDif = distance.Y;
            double xDif = distance.X;


            if (yDif < 0)
                yDif = -yDif;
            if (xDif < 0)
                xDif = -xDif;

            int aproxXDif = (int)xDif;
            int aproxYDif = (int)yDif;


            if (Math.Sqrt((xDif * xDif) + (yDif * yDif)) <= (13.5 + (27 * turretRange)))
            {
                return true;
            }
            else
                return false;
        }

        // Die method
        public void Die()
        {
            isActive = false;
            Waypoint = 15;
            player.Bubbles += reward;
            player.EnemiesLeft -= 1;
        }

        // Attack method
        public void Attack()
        {
            isActive = false;
            player.PlayerHealth -= damage;
            player.EnemiesLeft -= 1;
            if(player.PlayerHealth <= 0)
            {
                player.PlayerHealth = 0;
            }
        }
    }
}
