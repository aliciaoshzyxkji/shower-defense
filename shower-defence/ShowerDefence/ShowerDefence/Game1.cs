﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Audio;
using System.Collections.Generic;
using System;
using System.Threading;
using System.IO;
using System.Threading.Tasks;

/*
 * Shower Defence
 * Exterminator Studios
 * Team 9
 * GDAPS II  Monogame Game
 */

namespace ShowerDefence
{
    /// <summary>
    /// This is the main type for your game.
    /// </summary>
    public class Game1 : Game
    {

        GraphicsDeviceManager graphics;
        SpriteBatch spriteBatch;

        // mouse states
        MouseState mState;
        MouseState prevMState; // for 1-frame pressing

        // keyboard states
        KeyboardState kbState;
        KeyboardState prevKbState;

        // sounds
        SoundEffect music;
        SoundEffect lotionShoot;
        SoundEffect pillShoot;
        SoundEffect resume;
        SoundEffect gameOver;
        SoundEffect bubbles;

        //Backgrounds for game states

        //main menu 
        Texture2D mainMenuImage;

        //pause
        Texture2D pauseImage;

        //quit option
        Texture2D quitImage;

        //game
        Texture2D background;
        /*Background information - THIS IS THE GAMEPLAY BACKGROUND
         * Each tile is 27 x 27 pixels
         * The game play section of the screen is 21 tiles high and 32 tiles across
         * The bottom section for tower selection is 5 tiles tall 
         * The buttons are 4 tiles tall and 6 wide
         * 702 x 864
         */


        //End game
        Texture2D endScreenImage;

        //tower menus
        Texture2D lotionTowerMenuImage; // 324 pixels tall, 162 pixels wide, point at halfway on bottom
        Texture2D pillTowerMenuImage;
        Texture2D hairbrushTowerMenuImage;


        //enemy textures for testing
        //Texture2D spiderBase;
        Texture2D flyBase;

        //enemy attributes that will be read in from our external tools
        int antHealth;
        int flyHealth;
        int tarHealth;

        int antSpeed;
        int flySpeed;
        int tarSpeed;

        int playerHealth;
        int playerBubbles;

        // wave number
        int waveNumber = 1;

        // delay between spawning enemies in our wave
        int spawnDelay;

        //spritesheets of enemies 
        Texture2D antSS; // sprite sheet has 4 frames, each 11 pixels tall and 9 pixels wide
        Texture2D spiderSS; //sprite sheet has 5 frames, each 27x27 pixels, spider is centered to right
                            //  Texture2D flybase; // sprite sheet has 1 frame, 18 x 18 pixels 

        //towers
        Texture2D lotionBottleImage;    // 54x 54 pixels 
        Texture2D pillShooterImage;    // 54 x 54 pixels
        Texture2D hairbrushImage; /* sprite sheet has 10 frames, shifts top left, top right, 2nd row left, 2nd row right and so on
            each frame is 27 pixels tall and 54 pixels wide*/
                                  //Hairbrush currently sits horizontal with head facing right

        //tower projectiles
        Texture2D lotionImage; //10x10 pixels, faces right 
        Texture2D pillImage;  //15 x 15 pixels 

        //Menu Buttons
        Button gameStart;

        //in game buttons
        Button waveStart;
        Button pause;
        Button end;
        Button endSB;

        //other buttons
        Button unPause;
        Button yes;
        Button no;

        Button yesSB;
        Button noSB;

        // pause boolean
        bool paused = false;

        //tower menu mottons
        Button lMenu;
        Button pMenu;
        Button hMenu;
        Button lBuy;
        Button lSell;
        Button lU1;
        Button lU2;
        Button pBuy;
        Button pSell;
        Button pU1;
        Button pU2;
        Button hBuy;
        Button hSell;
        Button hU1;
        Button hU2;

        SpriteFont font;

        // list of enemies that will be spawned that wave
        List<Enemy> wave;
        // list of the current wave
        public List<Enemy> currentWave;

        //empty sprite 
        Texture2D tile; //27 x 27 pixels

        //button sprite
        Texture2D button;

        //animation testing for ant 
        //Ant a;

        // attribute for weapon for testing Milestone 2
        Weapon tower;

        // attributes for sounds
        bool newProgram = true;
        bool gameOverSound = true;

        // ready to start the next wave
        bool waveOver = true;

        //Tile array for grid system
        GridTile[,] tileArray = new GridTile[32, 21];

        // list of towers that you've already placed
        List<Weapon> towerList = new List<Weapon>();
        List<Weapon> lotionList = new List<Weapon>();
        List<Weapon> pillShootList = new List<Weapon>();
        List<Hairbrush> hairbrushList = new List<Hairbrush>();
        List<Weapon> upgradedLotionList = new List<Weapon>();
        List<Weapon> upgradedLotionList2 = new List<Weapon>();
        List<Weapon> upgradedPillShootList = new List<Weapon>();
        List<Weapon> upgradedPillShootList2 = new List<Weapon>();

        // list of tiles that have towers on them
        List<GridTile> weaponTiles = new List<GridTile>();

        // player
        Player player;

        enum GameState { Menu, Standby, Gameplay, GameOver, Pause, Quit, QuitSB } //Enum for the finite state system later on.
        GameState newGame;

        // property
        public List<Enemy> CurrentWave // property
        {
            get { return currentWave; }
            set { currentWave = value; }
        }

        public Game1()
        {
            graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";
        }
        //set up location of buttons 
        public void SetButtons()
        {
            LoadContent();
            gameStart = new Button(316, 441, 259, 91, button);
            waveStart = new Button(702, 0, 162, 108, button);
            pause = new Button(0, 0, 162, 54, button);
            end = new Button(0, 55, 162, 53, button);
            endSB = new Button(0, 55, 162, 53, button);
            unPause = new Button(0, 594, 864, 108, button);
            yes = new Button(108, 351, 320, 108, button);
            no = new Button(436, 351, 320, 108, button);
            yesSB = new Button(108, 351, 320, 108, button);
            noSB = new Button(436, 351, 320, 108, button);
            lMenu = new Button(0, 567, 281, 135, button);
            pMenu = new Button(293, 567, 281, 135, button);
            hMenu = new Button(580, 567, 287, 135, button);
            lBuy = new Button(92, 267, 65, 55, button);
            lSell = new Button(92, 332, 65, 55, button);
            lU1 = new Button(0, 436, 81, 52, button);
            lU2 = new Button(82, 436, 81, 52, button);
            pBuy = new Button(92, 267, 65, 55, button);
            pSell = new Button(92, 332, 65, 55, button);
            pU1 = new Button(0, 436, 81, 52, button);
            pU2 = new Button(82, 436, 81, 52, button);
            hBuy = new Button(92, 267, 65, 55, button);
            hSell = new Button(92, 332, 65, 55, button);
            hU1 = new Button(0, 436, 81, 52, button);
            hU2 = new Button(82, 436, 81, 52, button);


            //draw the menu @ 0, 243

        }

        /// <summary>
        /// Allows the game to perform any initialization it needs to before starting to run.
        /// This is where it can query for any required services and load any non-graphic
        /// related content.  Calling base.Initialize will enumerate through any components
        /// and initialize them as well.
        /// </summary>
        protected override void Initialize()
        {
            // TODO: Add your initialization logic here
            //make mouse visible
            this.IsMouseVisible = true;

            //allow window resizing
            this.Window.AllowUserResizing = true;

            // change the screen size
            graphics.PreferredBackBufferWidth = 864;
            graphics.PreferredBackBufferHeight = 702;
            graphics.ApplyChanges();

            // change the title
            this.Window.Title = "Shower Defence";

            // make first GameState the menu
            newGame = GameState.Menu;

            mState = Mouse.GetState();

            SetButtons();

            // load the enemy attributes from our external tool
            ReadFile();

            //Populate Tile array
            for (int x = 0; x < 32; x++)
            {
                for (int y = 0; y < 21; y++)
                {
                    Rectangle posPlaceHolder = new Rectangle(0 + (27 * x), 0 + (27 * y), 27, 27);
                    tileArray[x, y] = new GridTile(tile, posPlaceHolder, true, false);
                }
            }
            //Creating the path
            {
                //UI Tiles
                tileArray[0, 0].Placeable = false;
                tileArray[0, 1].Placeable = false;
                tileArray[0, 2].Placeable = false;
                tileArray[0, 3].Placeable = false;
                tileArray[1, 0].Placeable = false;
                tileArray[1, 1].Placeable = false;
                tileArray[1, 2].Placeable = false;
                tileArray[1, 3].Placeable = false;
                tileArray[2, 0].Placeable = false;
                tileArray[2, 1].Placeable = false;
                tileArray[2, 2].Placeable = false;
                tileArray[2, 3].Placeable = false;
                tileArray[3, 0].Placeable = false;
                tileArray[3, 1].Placeable = false;
                tileArray[3, 2].Placeable = false;
                tileArray[3, 3].Placeable = false;
                tileArray[4, 0].Placeable = false;
                tileArray[4, 1].Placeable = false;
                tileArray[4, 2].Placeable = false;
                tileArray[4, 3].Placeable = false;
                tileArray[5, 0].Placeable = false;
                tileArray[5, 1].Placeable = false;
                tileArray[5, 2].Placeable = false;
                tileArray[5, 3].Placeable = false;

                tileArray[26, 0].Placeable = false;
                tileArray[26, 1].Placeable = false;
                tileArray[26, 2].Placeable = false;
                tileArray[26, 3].Placeable = false;
                tileArray[27, 0].Placeable = false;
                tileArray[27, 1].Placeable = false;
                tileArray[27, 2].Placeable = false;
                tileArray[27, 3].Placeable = false;
                tileArray[28, 0].Placeable = false;
                tileArray[28, 1].Placeable = false;
                tileArray[28, 2].Placeable = false;
                tileArray[28, 3].Placeable = false;
                tileArray[29, 0].Placeable = false;
                tileArray[29, 1].Placeable = false;
                tileArray[29, 2].Placeable = false;
                tileArray[29, 3].Placeable = false;
                tileArray[30, 0].Placeable = false;
                tileArray[30, 1].Placeable = false;
                tileArray[30, 2].Placeable = false;
                tileArray[30, 3].Placeable = false;
                tileArray[31, 0].Placeable = false;
                tileArray[31, 1].Placeable = false;
                tileArray[31, 2].Placeable = false;
                tileArray[31, 3].Placeable = false;

                tileArray[14, 1].Placeable = false;
                tileArray[14, 2].Placeable = false;
                tileArray[15, 1].Placeable = false;
                tileArray[15, 2].Placeable = false;
                tileArray[16, 1].Placeable = false;
                tileArray[16, 2].Placeable = false;
                tileArray[17, 1].Placeable = false;
                tileArray[17, 2].Placeable = false;
                tileArray[18, 1].Placeable = false;
                tileArray[18, 2].Placeable = false;
                tileArray[19, 1].Placeable = false;
                tileArray[19, 2].Placeable = false;
                tileArray[20, 1].Placeable = false;
                tileArray[20, 2].Placeable = false;
                tileArray[21, 1].Placeable = false;
                tileArray[21, 2].Placeable = false;


                //Path 1
                tileArray[7, 0].Placeable = false;
                tileArray[7, 1].Placeable = false;
                tileArray[7, 2].Placeable = false;
                tileArray[7, 3].Placeable = false;
                tileArray[7, 4].Placeable = false;
                tileArray[7, 5].Placeable = false;
                tileArray[7, 6].Placeable = false;
                tileArray[7, 7].Placeable = false;
                tileArray[7, 8].Placeable = false;
                tileArray[7, 9].Placeable = false;
                tileArray[7, 10].Placeable = false;
                tileArray[7, 11].Placeable = false;
                tileArray[7, 12].Placeable = false;
                tileArray[7, 13].Placeable = false;
                tileArray[7, 14].Placeable = false;
                tileArray[7, 15].Placeable = false;
                tileArray[7, 16].Placeable = false;
                tileArray[7, 17].Placeable = false;
                tileArray[7, 18].Placeable = false;

                //Path 2
                tileArray[8, 18].Placeable = false;
                tileArray[9, 18].Placeable = false;
                tileArray[10, 18].Placeable = false;
                tileArray[11, 18].Placeable = false;
                tileArray[12, 18].Placeable = false;
                tileArray[13, 18].Placeable = false;
                tileArray[14, 18].Placeable = false;
                tileArray[15, 18].Placeable = false;
                tileArray[16, 18].Placeable = false;
                tileArray[17, 18].Placeable = false;
                tileArray[18, 18].Placeable = false;
                tileArray[19, 18].Placeable = false;
                tileArray[20, 18].Placeable = false;
                tileArray[21, 18].Placeable = false;
                tileArray[22, 18].Placeable = false;
                tileArray[23, 18].Placeable = false;
                tileArray[23, 18].Placeable = false;
                tileArray[23, 18].Placeable = false;

                //Path 3
                tileArray[23, 17].Placeable = false;
                tileArray[23, 16].Placeable = false;
                tileArray[23, 15].Placeable = false;
                tileArray[23, 14].Placeable = false;
                tileArray[23, 13].Placeable = false;

                //Path 4
                tileArray[22, 13].Placeable = false;
                tileArray[21, 13].Placeable = false;
                tileArray[20, 13].Placeable = false;
                tileArray[19, 13].Placeable = false;
                tileArray[18, 13].Placeable = false;
                tileArray[17, 13].Placeable = false;
                tileArray[16, 13].Placeable = false;
                tileArray[15, 13].Placeable = false;
                tileArray[14, 13].Placeable = false;
                tileArray[13, 13].Placeable = false;
                tileArray[12, 13].Placeable = false;
                tileArray[11, 13].Placeable = false;
                tileArray[10, 13].Placeable = false;

                //Path 5
                tileArray[10, 12].Placeable = false;
                tileArray[10, 11].Placeable = false;
                tileArray[10, 10].Placeable = false;
                tileArray[10, 9].Placeable = false;
                tileArray[10, 8].Placeable = false;
                tileArray[10, 7].Placeable = false;
                tileArray[10, 6].Placeable = false;
                tileArray[10, 5].Placeable = false;
                tileArray[10, 4].Placeable = false;

                //Path 6
                tileArray[11, 4].Placeable = false;
                tileArray[12, 4].Placeable = false;
                tileArray[13, 4].Placeable = false;
                tileArray[14, 4].Placeable = false;
                tileArray[15, 4].Placeable = false;

                //Path 7
                tileArray[15, 5].Placeable = false;
                tileArray[15, 6].Placeable = false;
                tileArray[15, 7].Placeable = false;
                tileArray[15, 8].Placeable = false;
                tileArray[15, 9].Placeable = false;
                tileArray[15, 10].Placeable = false;

                //Path 8
                tileArray[16, 10].Placeable = false;
                tileArray[17, 10].Placeable = false;
                tileArray[18, 10].Placeable = false;

                //Path 9
                tileArray[18, 9].Placeable = false;
                tileArray[18, 8].Placeable = false;
                tileArray[18, 7].Placeable = false;
                tileArray[18, 6].Placeable = false;
                tileArray[18, 5].Placeable = false;
                tileArray[18, 4].Placeable = false;

                //Path 10
                tileArray[19, 4].Placeable = false;
                tileArray[20, 4].Placeable = false;
                tileArray[21, 4].Placeable = false;
                tileArray[22, 4].Placeable = false;
                tileArray[23, 4].Placeable = false;

                //Path 11
                tileArray[23, 5].Placeable = false;
                tileArray[23, 6].Placeable = false;
                tileArray[23, 7].Placeable = false;
                tileArray[23, 8].Placeable = false;
                tileArray[23, 9].Placeable = false;
                tileArray[23, 10].Placeable = false;

                //Path 12
                tileArray[24, 10].Placeable = false;
                tileArray[25, 10].Placeable = false;
                tileArray[26, 10].Placeable = false;
                tileArray[27, 10].Placeable = false;

                //Shower
                for (int i = 6; i <= 16; i++)
                {
                    for (int j = 28; j <= 31; j++)
                    {
                        tileArray[j, i].Placeable = false;
                    }
                }

                base.Initialize();
            }
            foreach (GridTile tile in tileArray)
            {
                if (tile.Placeable == false)
                {
                    tile.HairbrushPlace = true;
                }
            }
            tileArray[0, 0].HairbrushPlace = false;
            tileArray[0, 1].HairbrushPlace = false;
            tileArray[0, 2].HairbrushPlace = false;
            tileArray[0, 3].HairbrushPlace = false;
            tileArray[1, 0].HairbrushPlace = false;
            tileArray[1, 1].HairbrushPlace = false;
            tileArray[1, 2].HairbrushPlace = false;
            tileArray[1, 3].HairbrushPlace = false;
            tileArray[2, 0].HairbrushPlace = false;
            tileArray[2, 1].HairbrushPlace = false;
            tileArray[2, 2].HairbrushPlace = false;
            tileArray[2, 3].HairbrushPlace = false;
            tileArray[3, 0].HairbrushPlace = false;
            tileArray[3, 1].HairbrushPlace = false;
            tileArray[3, 2].HairbrushPlace = false;
            tileArray[3, 3].HairbrushPlace = false;
            tileArray[4, 0].HairbrushPlace = false;
            tileArray[4, 1].HairbrushPlace = false;
            tileArray[4, 2].HairbrushPlace = false;
            tileArray[4, 3].HairbrushPlace = false;
            tileArray[5, 0].HairbrushPlace = false;
            tileArray[5, 1].HairbrushPlace = false;
            tileArray[5, 2].HairbrushPlace = false;
            tileArray[5, 3].HairbrushPlace = false;

            tileArray[26, 0].HairbrushPlace = false;
            tileArray[26, 1].HairbrushPlace = false;
            tileArray[26, 2].HairbrushPlace = false;
            tileArray[26, 3].HairbrushPlace = false;
            tileArray[27, 0].HairbrushPlace = false;
            tileArray[27, 1].HairbrushPlace = false;
            tileArray[27, 2].HairbrushPlace = false;
            tileArray[27, 3].HairbrushPlace = false;
            tileArray[28, 0].HairbrushPlace = false;
            tileArray[28, 1].HairbrushPlace = false;
            tileArray[28, 2].HairbrushPlace = false;
            tileArray[28, 3].HairbrushPlace = false;
            tileArray[29, 0].HairbrushPlace = false;
            tileArray[29, 1].HairbrushPlace = false;
            tileArray[29, 2].HairbrushPlace = false;
            tileArray[29, 3].HairbrushPlace = false;
            tileArray[30, 0].HairbrushPlace = false;
            tileArray[30, 1].HairbrushPlace = false;
            tileArray[30, 2].HairbrushPlace = false;
            tileArray[30, 3].HairbrushPlace = false;
            tileArray[31, 0].HairbrushPlace = false;
            tileArray[31, 1].HairbrushPlace = false;
            tileArray[31, 2].HairbrushPlace = false;
            tileArray[31, 3].HairbrushPlace = false;

            tileArray[14, 1].HairbrushPlace = false;
            tileArray[14, 2].HairbrushPlace = false;
            tileArray[15, 1].HairbrushPlace = false;
            tileArray[15, 2].HairbrushPlace = false;
            tileArray[16, 1].HairbrushPlace = false;
            tileArray[16, 2].HairbrushPlace = false;
            tileArray[17, 1].HairbrushPlace = false;
            tileArray[17, 2].HairbrushPlace = false;
            tileArray[18, 1].HairbrushPlace = false;
            tileArray[18, 2].HairbrushPlace = false;
            tileArray[19, 1].HairbrushPlace = false;
            tileArray[19, 2].HairbrushPlace = false;
            tileArray[20, 1].HairbrushPlace = false;
            tileArray[20, 2].HairbrushPlace = false;
            tileArray[21, 1].HairbrushPlace = false;
            tileArray[21, 2].HairbrushPlace = false;


            base.Initialize();
        }

        /// <summary>
        /// LoadContent will be called once per game and is the place to load
        /// all of your content.
        /// </summary>
        protected override void LoadContent()
        {
            // Create a new SpriteBatch, which can be used to draw textures.
            spriteBatch = new SpriteBatch(GraphicsDevice);

            // TODO: use this.Content to load your game content here
            //testing image
            //spiderBase = Content.Load<Texture2D>("spiderBase");

            //screens
            mainMenuImage = Content.Load<Texture2D>("mainMenu");
            pauseImage = Content.Load<Texture2D>("pause");
            background = Content.Load<Texture2D>("UI Base");
            endScreenImage = Content.Load<Texture2D>("EndScreen");
            quitImage = Content.Load<Texture2D>("quitScreen");

            //menu
            lotionTowerMenuImage = Content.Load<Texture2D>("tower menu");
            pillTowerMenuImage = Content.Load<Texture2D>("tower menu 2");
            hairbrushTowerMenuImage = Content.Load<Texture2D>("tower menu 3");

            //enemies
            antSS = Content.Load<Texture2D>("antSS");
            flyBase = Content.Load<Texture2D>("flyBase");
            spiderSS = Content.Load<Texture2D>("spiderSS");

            //towers
            lotionBottleImage = Content.Load<Texture2D>("lotionBottle");
            hairbrushImage = Content.Load<Texture2D>("hairbrushSS");
            pillShooterImage = Content.Load<Texture2D>("pillShooter");

            //projectiles
            pillImage = Content.Load<Texture2D>("pillCapsole");
            lotionImage = Content.Load<Texture2D>("lotion");

            //invisible sprite 
            tile = Content.Load<Texture2D>("invisibleSprite");

            //button 
            button = Content.Load<Texture2D>("border");

            //font
            font = Content.Load<SpriteFont>("Arial");

            /*music = Content.Load<SoundEffect>("GameMusic");
            lotionShoot = Content.Load<SoundEffect>("LotionShoot");
            pillShoot = Content.Load<SoundEffect>("PillSound");
            gameOver = Content.Load<SoundEffect>("GameOver");
            bubbles = Content.Load<SoundEffect>("Bubbles");
            resume = Content.Load<SoundEffect>("StartGame");*/

            try
            {
                if (newProgram)
                {
                    SoundEffectInstance musicInstance = music.CreateInstance();
                    musicInstance.IsLooped = true;
                    musicInstance.Volume = 0.01f;
                    musicInstance.Play();
                    newProgram = false;
                }
            }
            catch { }
        }

        /// <summary>
        /// UnloadContent will be called once per game and is the place to unload
        /// game-specific content.
        /// </summary>
        protected override void UnloadContent()
        {
            // TODO: Unload any non ContentManager content here
        }

        /// <summary>
        /// Allows the game to run logic such as updating the world,
        /// checking for collisions, gathering input, and playing audio.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Update(GameTime gameTime)
        {
            if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed || Keyboard.GetState().IsKeyDown(Keys.Escape))
                Exit();

            // TODO: Add your update logic here

            base.Update(gameTime);

            if (newGame == GameState.Gameplay)
            {

                if (player.PlayerHealth <= 0) // player lost the game
                {
                    newGame = GameState.GameOver;
                }

                // the wave is over
                if (player.EnemiesLeft == 0)
                {
                    waveOver = true;
                    newGame = GameState.Standby;
                    wave.Clear();
                    currentWave.Clear();
                    waveStart.Pressed = false;
                }
            }
            // assign previous mouse and keyboard states to current ones
            prevMState = mState;
            prevKbState = kbState;
            mState = Mouse.GetState();
            kbState = Keyboard.GetState();

            // switch block to update the GameState
            switch (newGame)
            {
                case GameState.Menu:
                    // switching between menu and standby
                    //update game start button 

                    gameStart.IsActive = true;
                    gameStart.Update(mState, prevMState);

                    if (gameStart.Pressed == true)
                    {
                        gameStart.IsActive = false;
                        gameStart.Pressed = false;
                        newGame = GameState.Standby;
                        waveStart.IsActive = true;
                        end.IsActive = true;

                        // sound
                        try
                        {
                            SoundEffectInstance soundInstance = resume.CreateInstance();
                            soundInstance.Volume = 0.1f;
                            soundInstance.Play();
                        }
                        catch { }

                        // initialize the wave
                        wave = new List<Enemy>();
                        currentWave = new List<Enemy>();

                        // make the player
                        player = new Player(playerHealth, playerBubbles);

                        // set the spawn delay for the enemies and their spawning
                        spawnDelay = 30;

                        waveOver = true;
                    }

                    /* if (mState.RightButton == ButtonState.Pressed && prevMState.RightButton == ButtonState.Released)
                     {
                         newGame = GameState.Standby;
                     }
                     */
                    break;
                case GameState.Pause:
                    //activate buttons 
                    unPause.IsActive = true;
                    unPause.Update(mState, prevMState);
                    paused = true;
                    foreach (Enemy enemy in currentWave)
                    {
                        if(enemy.Paused == false)
                        {
                            enemy.Paused = true; // make the enemies stop moving
                            enemy.TempWaypoint = enemy.Waypoint;
                            enemy.Waypoint = 15;
                        }
                    }
                    foreach (Weapon tower in towerList)
                    {
                        tower.Paused = true; // make the towers stop ing
                    }

                    if (unPause.Pressed == true) // resume game back to normal
                    {
                        unPause.Pressed = false;
                        unPause.IsActive = false;
                        newGame = GameState.Gameplay; // sets back to gameplay, however, if the player is really in standby
                                                      // it'll still work fine since the enemy count is 0 and will put the player right
                                                      // back into standby
                        paused = false;
                        foreach (Enemy enemy in currentWave)
                        {
                            enemy.Paused = false;
                            enemy.Waypoint = enemy.TempWaypoint;
                            enemy.Move(tileArray);
                        }
                        foreach (Weapon tower in towerList)
                        {
                            tower.Paused = false;
                        }

                        try
                        {
                            SoundEffectInstance soundInstance = resume.CreateInstance();
                            soundInstance.Volume = 0.1f;
                            soundInstance.Play();
                        }
                        catch { }
                    }

                    break;
                case GameState.Quit: // for gameplay state

                    //activate buttons 
                    yes.IsActive = true;
                    no.IsActive = true;

                    yes.Update(mState, prevMState);
                    no.Update(mState, prevMState);

                    paused = true;

                    foreach (Enemy enemy in currentWave)
                    {
                        if (enemy.Paused == false)
                        {
                            enemy.Paused = true; // make the enemies stop moving
                            enemy.TempWaypoint = enemy.Waypoint;
                            enemy.Waypoint = 15;
                        }
                    }
                    foreach (Weapon tower in towerList)
                    {
                        tower.Paused = true; // make the towers stop ing
                    }

                    if (yes.Pressed == true)
                    {

                        no.Pressed = false;
                        yes.IsActive = false;
                        no.IsActive = false;
                        newGame = GameState.Gameplay;
                        paused = false;
                        foreach (Enemy enemy in currentWave)
                        {
                            enemy.Paused = false;
                            enemy.Waypoint = enemy.TempWaypoint;
                            enemy.Move(tileArray);
                        }
                        foreach (Weapon tower in towerList)
                        {
                            tower.Paused = false;
                        }

                        yes.Pressed = false;
                        yes.IsActive = false;
                        no.IsActive = false;
                        newGame = GameState.GameOver;
                    }

                    if (no.Pressed == true)
                    {
                        no.Pressed = false;
                        yes.IsActive = false;
                        no.IsActive = false;
                        newGame = GameState.Gameplay;
                        paused = false;
                        foreach (Enemy enemy in currentWave)
                        {
                            enemy.Paused = false;
                            enemy.Waypoint = enemy.TempWaypoint;
                            enemy.Move(tileArray);
                        }
                        foreach (Weapon tower in towerList)
                        {
                            tower.Paused = false;
                        }

                        try
                        {
                            SoundEffectInstance soundInstance = resume.CreateInstance();
                            soundInstance.Volume = 0.1f;
                            soundInstance.Play();
                        }
                        catch { }
                    }

                    break;
                case GameState.QuitSB:

                    yesSB.IsActive = true;
                    noSB.IsActive = true;

                    yesSB.Update(mState, prevMState);
                    noSB.Update(mState, prevMState);

                    if (yesSB.Pressed == true)
                    {
                        yesSB.Pressed = false;
                        yesSB.IsActive = false;
                        noSB.IsActive = false;
                        newGame = GameState.GameOver;
                    }

                    if (noSB.Pressed == true)
                    {
                        noSB.Pressed = false;
                        yesSB.IsActive = false;
                        noSB.IsActive = false;
                        newGame = GameState.Standby;

                        try
                        {
                            SoundEffectInstance soundInstance = resume.CreateInstance();
                            soundInstance.Volume = 0.1f;
                            soundInstance.Play();
                        }
                        catch { }
                    }
                    break;
                case GameState.Gameplay:

                    TowerDrawing();

                    //make sure buttons are active
                    end.IsActive = true;
                    pause.IsActive = true;

                    end.Update(mState, prevMState);
                    pause.Update(mState, prevMState);

                    // switching between gameplay and quit screen
                    if (end.Pressed == true)
                    {
                        end.Pressed = false;
                        end.IsActive = false;
                        pause.IsActive = false;
                        newGame = GameState.Quit;

                    }

                    //switching between gameplay and pause
                    if (pause.Pressed == true)
                    {
                        pause.Pressed = false;
                        end.IsActive = false;
                        pause.IsActive = false;
                        newGame = GameState.Pause;

                    }
                    foreach (Enemy bug in currentWave)
                    {
                        if (bug != null)
                        {
                            bug.Update(gameTime);
                        }
                    }

                    break;

                case GameState.Standby:

                    TowerDrawing();

                    // switching between standby and end game
                    if (end.Pressed == true)
                    {
                        end.Pressed = false;
                        waveStart.IsActive = false;
                        end.IsActive = false;
                        pause.IsActive = false;
                        newGame = GameState.Quit;

                    }

                    // switching between Standby and Gameplay
                    if (waveStart.Pressed == true && waveOver == true)
                    {

                        waveStart.Pressed = false;
                        waveStart.IsActive = false;
                        lMenu.IsActive = false;
                        pMenu.IsActive = false;
                        hMenu.IsActive = false;
                        end.IsActive = true;
                        pause.IsActive = true;
                        newGame = GameState.Gameplay;

                        // this is where we'll generate the waves with what we want
                        switch (waveNumber)
                        {
                            case 1:
                                Populate(3, 0, 0);
                                waveNumber++;
                                break;
                            case 2:
                                Populate(5, 0, 0);
                                waveNumber++;
                                break;
                            case 3:
                                Populate(8, 0, 0);
                                waveNumber++;
                                break;
                            case 4:
                                Populate(4, 3, 0);
                                waveNumber++;
                                break;
                            case 5:
                                Populate(10, 5, 0);
                                waveNumber++;
                                break;
                            case 6:
                                Populate(12, 6, 0);
                                waveNumber++;
                                break;
                            case 7:
                                Populate(14, 7, 0);
                                waveNumber++;
                                break;
                            case 8:
                                Populate(16, 8, 0);
                                waveNumber++;
                                break;
                            case 9:
                                Populate(20, 6, 0);
                                waveNumber++;
                                break;
                            case 10:
                                Populate(12, 0, 1);
                                waveNumber++;
                                break;
                            case 11:
                                Populate(10, 8, 1);
                                waveNumber++;
                                break;
                            case 12:
                                Populate(22, 10, 0);
                                waveNumber++;
                                break;
                            case 13:
                                Populate(0, 20, 0);
                                waveNumber++;
                                break;
                            case 14:
                                Populate(20, 10, 1);
                                waveNumber++;
                                break;
                            case 15:
                                Populate(15, 5, 2);
                                waveNumber++;
                                break;
                            case 16:
                                Populate(24, 12, 0);
                                waveNumber++;
                                break;
                            case 17:
                                Populate(27, 12, 0);
                                waveNumber++;
                                break;
                            case 18:
                                Populate(5, 5, 3);
                                waveNumber++;
                                break;
                            case 19:
                                Populate(10, 20, 0);
                                waveNumber++;
                                break;
                            case 20:
                                Populate(40, 0, 1);
                                waveNumber++;
                                break;
                            case 21:
                                Populate(22, 15, 3);
                                waveNumber++;
                                break;
                            case 22:
                                Populate(30, 17, 2);
                                waveNumber++;
                                break;
                            case 23:
                                Populate(30, 18, 3);
                                waveNumber++;
                                break;
                            case 24:
                                Populate(30, 20, 4);
                                waveNumber++;
                                break;
                            case 25:
                                Populate(33, 21, 4);
                                waveNumber++;
                                break;
                            default:
                                Populate(Convert.ToInt32(waveNumber * 1.05), Convert.ToInt32((waveNumber - 10) * 1.05), Convert.ToInt32((waveNumber - 25) * 1.05));
                                waveNumber++;
                                break;
                        }
                        try
                        {
                            SoundEffectInstance soundInstance = resume.CreateInstance();
                            soundInstance.Volume = 0.1f;
                            soundInstance.Play();
                        }
                        catch { }
                        waveOver = false;
                    }

                    break;

                case GameState.GameOver:

                    // sound
                    try
                    {
                        if(gameOverSound)
                        {
                            SoundEffectInstance soundInstance = gameOver.CreateInstance();
                            soundInstance.Volume = 0.1f;
                            soundInstance.Play();
                            gameOverSound = false;
                        }
                    }
                    catch { }

                    SetButtons();
                    towerList.Clear();
                    lotionList.Clear();
                    upgradedLotionList.Clear();
                    upgradedLotionList2.Clear();
                    pillShootList.Clear();
                    upgradedPillShootList.Clear();
                    upgradedPillShootList2.Clear();
                    wave.Clear();
                    currentWave.Clear();
                    waveNumber = 1;
                    waveOver = true;
                    paused = false;
                    if (mState.LeftButton == ButtonState.Pressed && prevMState.LeftButton == ButtonState.Released)
                    {
                        newGame = GameState.Menu;
                        gameOverSound = true;
                    }
                    break;
            }
        }

        /// <summary>
        /// This is called when the game should draw itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.Black);

            // TODO: Add your drawing code here

            base.Draw(gameTime);
            spriteBatch.Begin();

            //Finite state system for drawing and animation based on game state

            if (newGame == GameState.Menu)
            {
                gameStart.IsActive = true;
                spriteBatch.Draw(mainMenuImage, new Vector2(0.0f, 0.0f), Color.White);
                gameStart.Draw(spriteBatch);
                towerList.Clear();
                lotionList.Clear();
                pillShootList.Clear();
                hairbrushList.Clear();
                waveNumber = 1;
                wave = new List<Enemy>();
                currentWave = new List<Enemy>();
            }
            else if (newGame == GameState.Pause)
            {
                spriteBatch.Draw(pauseImage, new Vector2(0.0f, 0.0f), Color.White);
            }
            else if (newGame == GameState.Quit)
            {
                spriteBatch.Draw(quitImage, new Vector2(0.0f, 0.0f), Color.White);
            }
            else if (newGame == GameState.QuitSB)
            {
                spriteBatch.Draw(quitImage, new Vector2(0.0f, 0.0f), Color.White);
            }
            else if (newGame == GameState.GameOver)
            {
                spriteBatch.Draw(endScreenImage, new Vector2(0.0f, 0.0f), Color.White);
            }
            else if (newGame == GameState.Standby)
            {

                // draw the background
                spriteBatch.Draw(background, new Vector2(0.0f, 0.0f), Color.White);

                end.Draw(spriteBatch);
                waveStart.Draw(spriteBatch);

                //draw player health and bubbles // edge of space 351 x, 27 y,
                spriteBatch.DrawString(font, "Bubbles : " + player.Bubbles, new Vector2(370, 25), Color.Black);
                spriteBatch.DrawString(font, "Lives : " + player.PlayerHealth, new Vector2(370, 53), Color.Black);

                //tower purchasing 
                spriteBatch.DrawString(font, "200 bubbles", new Vector2(110, 658), Color.Black);
                spriteBatch.DrawString(font, "200 bubbles", new Vector2(403, 658), Color.Black);

                spriteBatch.DrawString(font, "150 bubbles", new Vector2(690, 658), Color.Black);

                spriteBatch.DrawString(font, "150 bubbles", new Vector2(690, 658), Color.Black);

                BuyingTowers();

            }

            // for spawning the background and enemies
            else if (newGame == GameState.Gameplay)
            {


                spriteBatch.Draw(background, new Vector2(0.0f, 0.0f), Color.White);

                pause.Draw(spriteBatch);
                end.Draw(spriteBatch);


                //draw player health and bubbles // edge of space 351 x, 27 y,
                spriteBatch.DrawString(font, "Bubbles : " + player.Bubbles, new Vector2(370, 25), Color.Black);
                spriteBatch.DrawString(font, "Lives : " + player.PlayerHealth, new Vector2(370, 53), Color.Black);

                //tower purchasing 
                spriteBatch.DrawString(font, "200 bubbles", new Vector2(110, 658), Color.Black);
                spriteBatch.DrawString(font, "200 bubbles", new Vector2(403, 658), Color.Black);
                spriteBatch.DrawString(font, "150 bubbles", new Vector2(690, 658), Color.Black);

                BuyingTowers();

                try
                {
                    foreach (Enemy bug in currentWave)
                    {
                        bug.Draw(spriteBatch, gameTime);

                        foreach (Weapon tow in towerList)
                        {

                            if (tow.Firing == false && tow.Regen == false)
                            {
                                tow.ShotsPerSecond();
                                tow.Regen = true;
                            }

                            if (tow is Lotion || tow is PillShoot)
                            {
                                if (bug.InRange(tow.Rect, tow.Range) == true && bug.IsFlying == tow.AirShot && tow.Target == null && bug.IsActive == true)
                                {
                                    tow.Target = bug;
                                }
                                if (tow.Target != null)
                                {
                                    if (tow.Target.InRange(tow.Rect, tow.Range) == false)
                                    {
                                        tow.Target = null;
                                    }
                                }
                                if (tow is Lotion)
                                {
                                    if (tow.AirShot == bug.IsFlying && tow.Range != -1 && tow.Firing == true && tow.Target != null)
                                        tow.Shoot(tow.Target, spriteBatch, tow.BulletImage, lotionShoot);
                                }
                                else
                                {
                                    if (tow.AirShot == bug.IsFlying && tow.Range != -1 && tow.Firing == true && tow.Target != null)
                                        tow.Shoot(tow.Target, spriteBatch, tow.BulletImage, pillShoot);
                                }
                            }

                            if (tow is Hairbrush)
                            {
                                Hairbrush hb = (Hairbrush)tow;
                                if (bug.IsFlying == false)
                                {
                                    if (tow.RectX + 13 == bug.posX && tow.RectY + 13 == bug.posY)
                                    {
                                        if (tow.Firing == true)
                                        {
                                            tow.Firing = false;
                                            hb.RunOver(gameTime, bug, bug.posX, bug.posY);
                                            if (hb.Bristles == 0)
                                            {
                                                towerList.Remove(hb);
                                            }
                                            break;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                catch { }

                    /*
                    foreach (Weapon turret in towerList)
                    {

                        foreach (Enemy b in currentWave)
                        {
                            if (turret is Lotion || turret is PillShoot)
                            {
                                if (b.InRange(turret.Rect, turret.Range) == true && b.IsFlying == turret.AirShot && turret.Target == null && b.IsActive == true)
                                {
                                    turret.Target = b;
                                }
                                if (turret.Target != null)
                                {
                                    if (turret.Target.InRange(turret.Rect, turret.Range) == false)
                                    {
                                        turret.Target = null;
                                    }
                                }
                                if (turret is Lotion)
                                {
                                    if (turret.AirShot == b.IsFlying && turret.Range != -1 && turret.Firing == true && turret.Target != null)
                                        turret.Shoot(turret.Target, spriteBatch, turret.BulletImage, lotionShoot);
                                }
                                else
                                {
                                    if (turret.AirShot == b.IsFlying && turret.Range != -1 && turret.Firing == true && turret.Target != null)
                                        turret.Shoot(turret.Target, spriteBatch, turret.BulletImage, pillShoot);
                                }
                            }
                        }
                        
                    }
                    */
                }
            spriteBatch.End();
        }

        // method to populate the list of enemies that spawn
        protected async void Populate(int ants, int flies, int spiders)
        {
            // our enemies
            Enemy ant;
            Enemy fly;
            Enemy spider;

            spawnDelay -= Convert.ToInt32(spawnDelay * 0.05);

            if(spawnDelay <= 0)
            {
                spawnDelay = 1;
            }

            // making an ant 2 times
            LoadContent();

            // populate the wave with the passed in information
            for (int i = 0; i < ants; i++)
            {
                ant = new Ant(Convert.ToInt32(antHealth + (waveNumber * (antHealth * 0.05))), antSpeed, 1, 25, false, antSS, new Vector2(tileArray[7, 0].PosX + 13.5f, tileArray[0, 0].PosY + 13.5f), new Point(9, 11), 4, 40, new Point(0, 0), 1, player);
                wave.Add(ant);
                player.EnemiesLeft += 1;
            }
            for (int i = 0; i < flies; i++)
            {
                fly = new Fly(Convert.ToInt32(flyHealth + (waveNumber * (flyHealth * 0.05))), flySpeed, 1, 30, true, flyBase, new Vector2(tileArray[7, 0].PosX + 13.5f, tileArray[0, 0].PosY + 13.5f), new Point(18, 18), 4, 40, new Point(0, 0), player);
                wave.Add(fly);
                player.EnemiesLeft += 1;
            }
            for (int i = 0; i < spiders; i++)
            {
                spider = new Tarantula(Convert.ToInt32(tarHealth + (waveNumber * (tarHealth * 0.25))), tarSpeed, 5, 100, false, spiderSS, new Vector2(tileArray[7, 0].PosX + 13.5f, tileArray[0, 0].PosY + 13.5f), new Point(27, 27), 5, 500, new Point(0, 0), player);
                wave.Add(spider);
                player.EnemiesLeft += 1;
            }

            for (int i = 0; i < wave.Count; i++) // add what's been added to the currentwave
            {
                for (int j = 0; j < 30;)
                {
                    if (newGame == GameState.GameOver || newGame == GameState.Menu) // make sure the user didn't end the game
                    {
                        return;
                    }
                    else
                    {
                        if (!paused) // while spawning the enemies in delay, check to see if the game paused 50 times during the two second delay
                        {
                            await Task.Delay(spawnDelay); // if not than continue the delay to spawn the next enemy
                            j++; // if it is paused, than this loop while continue and do nothing until the game is resumed
                        }
                    }
                }
                currentWave.Add((wave[i]));
                currentWave[i].Move(tileArray); // then move those enemies to their first waypoint
            }
            wave.Clear();
        }

        // for drawing the towers
        public void TowerDrawing()
        {
            //buttons
            waveStart.IsActive = true;
            end.IsActive = true;
            pause.IsActive = true;
            lMenu.IsActive = true;
            pMenu.IsActive = true;
            hMenu.IsActive = true;

            //update buttons
            end.Update(mState, prevMState);
            pause.Update(mState, prevMState);
            waveStart.Update(mState, prevMState);
            lMenu.Update(mState, prevMState);
            pMenu.Update(mState, prevMState);
            hMenu.Update(mState, prevMState);

            //lotion menu stuff
            //if it is pressed and isnt being drawn 
            if (lMenu.Pressed == true && lMenu.Dr == false)
            {
                //reset pressed
                lMenu.Pressed = false;
                //make it draw
                lMenu.Dr = true;
                hMenu.Dr = false;
                pMenu.Dr = false;
                lBuy.Pressed = false;
                pBuy.Pressed = false;
                hBuy.Pressed = false;
            }
            //if it is already drawn 
            if (lMenu.Pressed == true && lMenu.Dr == true)
            {
                //reset pressed
                lMenu.Pressed = false;
                //make it stop drawing
                lMenu.Dr = false;
                hMenu.Dr = false;
                pMenu.Dr = false;
                lBuy.Pressed = false;
                pBuy.Pressed = false;
                hBuy.Pressed = false;
            }
            if (lMenu.Dr == true)
            {
                //activate and update buttons
                lBuy.IsActive = true;
                lBuy.Update(mState, prevMState);
                lSell.IsActive = true;
                lSell.Col = Color.Red;
                lSell.Update(mState, prevMState);
                lU1.IsActive = true;
                lU1.Col = Color.Red;
                lU1.Update(mState, prevMState);
                lU2.IsActive = true;
                lU2.Col = Color.Red;
                lU2.Update(mState, prevMState);
                //place any button code here 
            }
            else
            {
                lMenu.Pressed = false;
                lMenu.Dr = false;
                lBuy.IsActive = false;
                lSell.IsActive = false;
                lU1.IsActive = false;
                lU2.IsActive = false;
            }

            //Pill shooter menu stuff
            if (pMenu.Pressed == true && pMenu.Dr == false)
            {
                pMenu.Pressed = false;
                pMenu.Dr = true;
                lMenu.Dr = false;
                hMenu.Dr = false;
                lBuy.Pressed = false;
                pBuy.Pressed = false;
                hBuy.Pressed = false;
            }
            if (pMenu.Pressed == true && pMenu.Dr == true)
            {
                pMenu.Pressed = false;
                pMenu.Dr = false;
                lMenu.Dr = false;
                hMenu.Dr = false;
                lBuy.Pressed = false;
                pBuy.Pressed = false;
                hBuy.Pressed = false;
            }
            if (pMenu.Dr == true)
            {
                //activate and update buttons
                pBuy.IsActive = true;
                pBuy.Update(mState, prevMState);
                pSell.IsActive = true;
                pSell.Col = Color.Red;
                pSell.Update(mState, prevMState);
                pU1.IsActive = true;
                pU1.Col = Color.Red;
                pU1.Update(mState, prevMState);
                pU2.IsActive = true;
                pU2.Col = Color.Red;
                pU2.Update(mState, prevMState);
                //place any button code here 
            }
            else
            {
                pMenu.Pressed = false;
                pMenu.Dr = false;
                pBuy.IsActive = false;
                pSell.IsActive = false;
                pU1.IsActive = false;
                pU2.IsActive = false;
            }

            //Hairbrush menu stuff
            if (hMenu.Pressed == true && hMenu.Dr == false)
            {
                hMenu.Pressed = false;
                hMenu.Dr = true;
                lMenu.Dr = false;
                pMenu.Dr = false;
                lBuy.Pressed = false;
                pBuy.Pressed = false;
                hBuy.Pressed = false;
            }
            if (hMenu.Pressed == true && hMenu.Dr == true)
            {
                hMenu.Pressed = false;
                hMenu.Dr = false;
                lMenu.Dr = false;
                pMenu.Dr = false;
                lBuy.Pressed = false;
                pBuy.Pressed = false;
                hBuy.Pressed = false;
            }
            if (hMenu.Dr == true)
            {
                //activate and update buttons
                hBuy.IsActive = true;
                hBuy.Update(mState, prevMState);
                hSell.IsActive = true;
                hSell.Col = Color.Red;
                hSell.Update(mState, prevMState);
                hU1.IsActive = true;
                hU1.Col = Color.Red;
                hU1.Update(mState, prevMState);
                hU2.IsActive = true;
                hU2.Col = Color.Red;
                hU2.Update(mState, prevMState);
                //place any button code here 

            }
            else
            {
                hMenu.Pressed = false;
                hMenu.Dr = false;
                hBuy.IsActive = false;
                hSell.IsActive = false;
                hU1.IsActive = false;
                hU2.IsActive = false;
            }

            if (player.Bubbles > 0)
            {
                // adding to your list of towers upon click (if not on an illegal tile)
                if ((lBuy.Pressed == true && player.Bubbles >= 200)|| (pBuy.Pressed == true && player.Bubbles >= 200) || (hBuy.Pressed == true && player.Bubbles >= 150))
                {
                    lSell.Pressed = false;
                    lU1.Pressed = false;
                    lU1.Col = Color.Red;
                    lU2.Pressed = false;
                    lU2.Col = Color.Red;
                    pSell.Pressed = false;
                    if (tower != null)
                    {
                        if (mState.LeftButton == ButtonState.Pressed && prevMState.LeftButton == ButtonState.Released && tower.Clr != Color.Red)
                        {
                            //subtract bubbles for lotion
                            if (lBuy.Pressed == true && player.Bubbles >= 200)
                            {
                                player.Bubbles -= 200;
                                lBuy.Pressed = false;
                                towerList.Add(tower);
                                lotionList.Add(tower);
                                try
                                {
                                    SoundEffectInstance soundInstance = bubbles.CreateInstance();
                                    soundInstance.Volume = 0.1f;
                                    soundInstance.Play();
                                }
                                catch { }
                            }
                            //subtract bubbles from pill shooter
                            if (pBuy.Pressed == true && player.Bubbles >= 200)
                            {
                                player.Bubbles -= 200;
                                pBuy.Pressed = false;
                                towerList.Add(tower);
                                pillShootList.Add(tower);
                                try
                                {
                                    SoundEffectInstance soundInstance = bubbles.CreateInstance();
                                    soundInstance.Volume = 0.1f;
                                    soundInstance.Play();
                                }
                                catch { }
                            }
                            //subtract bubbles for hairbrush 
                            if (hBuy.Pressed == true && player.Bubbles >= 150)
                            {
                                player.Bubbles -= 150;
                                hBuy.Pressed = false;
                                towerList.Add(tower);
                                try
                                {
                                    SoundEffectInstance soundInstance = bubbles.CreateInstance();
                                    soundInstance.Volume = 0.1f;
                                    soundInstance.Play();
                                }
                                catch { }

                            }
                        }
                    }
                }
            }
        }

        public void BuyingTowers()
        {
            GameTime gameTime = new GameTime();

            if (lBuy.Pressed == true && player.Bubbles >= 200)
            {
                int x = -100;
                int y = -100;
                // searching the entire grid
                for (int i = 0; i <= 31; i++)
                {
                    // int count = 0;
                    for (int j = 0; j <= 20; j++)
                    {
                        if (new Rectangle(mState.X, mState.Y, 1, 1).Intersects(tileArray[i, j].Pos))
                        {
                            x = tileArray[i, j].PosX;
                            y = tileArray[i, j].PosY;
                        }
                    }
                }
                // initiate the weapon that hangs onto the mouse
                tower = new Lotion(0, 3, 3, 0, 1, false, lotionBottleImage, new Rectangle(x, y, 54, 54), Color.White, lotionImage);
                
            }
            if (pBuy.Pressed == true && player.Bubbles >= 200)
            {
                int x = -100;
                int y = -100;
                // searching the entire grid
                for (int i = 0; i <= 31; i++)
                {
                    // int count = 0;
                    for (int j = 0; j <= 20; j++)
                    {
                        if (new Rectangle(mState.X, mState.Y, 1, 1).Intersects(tileArray[i, j].Pos))
                        {
                            x = tileArray[i, j].PosX;
                            y = tileArray[i, j].PosY;
                        }
                    }
                    // initiate the weapon that hangs onto the mouse
                    tower = new PillShoot(0, 3, 3, 0, 1.0, true, pillShooterImage, new Rectangle(x, y, 54, 54), Color.White, pillImage);
                }
            }
            if (hBuy.Pressed == true && player.Bubbles >= 150)
            {
                int x = -100;
                int y = -100;
                // searching the entire grid
                for (int i = 0; i <= 31; i++)
                {
                    // int count = 0;
                    for (int j = 0; j <= 20; j++)
                    {
                        if (new Rectangle(mState.X, mState.Y, 1, 1).Intersects(tileArray[i, j].Pos))
                        {
                            x = tileArray[i, j].PosX;
                            y = tileArray[i, j].PosY;
                        }
                    }
                    // initiate the weapon that hangs onto the mouse
                    Rectangle r = new Rectangle(x, y, 27, 54);
                    tower = new Hairbrush(0, 2, 3, 0, 50, false, hairbrushImage, r, Color.Transparent);
                    Hairbrush h = (Hairbrush)tower;
                    hairbrushList.Add(h);
                    h.Col = Color.White;
                    h.Update(gameTime, tower.RectX, tower.RectY);
                }
            }

            if (lBuy.Pressed == true || pBuy.Pressed == true || hBuy.Pressed == true)
            {
                tower.Place(tower, mState);

                // showing the towers you placed
                foreach (Weapon tow in towerList)
                {
                    if (tow.Sold != true)
                    {
                        // checks to see if the tower on the mouse is intersecting with a tower that's already placed
                        if (tower.Rect.Intersects(tow.Rect))
                        {

                            tower.Clr = Color.Red;
                            
                            if (tower is Hairbrush)
                            {
                                Hairbrush hb = (Hairbrush)tower;
                                hb.Col = Color.Red;
                            }
                            
                        }
                        
                        // drawing according to the tiles that were clicked
                        if (tow is Hairbrush)
                        {
                            Hairbrush hb = (Hairbrush)tow;
                            //hb.Col = Color.White;
                            hb.Draw(spriteBatch, gameTime, tow.RectX, tow.RectY);
                            try
                            {
                                foreach (Enemy bug in currentWave)
                                {
                                    if (bug.IsFlying == false)
                                    {
                                        if (tow.RectX + 13 == bug.posX && tow.RectY + 13 == bug.posY)
                                        {
                                            if (tow.Firing == true)
                                            {
                                                tow.Firing = false;
                                                hb.RunOver(gameTime, bug, bug.posX, bug.posY);
                                                if (hb.Bristles == 0)
                                                {
                                                    towerList.Remove(hb);
                                                    BuyingTowers();
                                                    return;
                                                }
                                                break;
                                            }
                                        }
                                    }
                                }
                            }
                            catch { }
                            
                            continue;
                        }
                        if(tow.UpgrdLvl == 0)
                        {
                            spriteBatch.Draw(tow.Image, tow.Rect, Color.White);
                        }
                        if(tow.UpgrdLvl == 1)
                        {
                            spriteBatch.Draw(tow.Image, tow.Rect, Color.LightSalmon);
                        }
                        if (tow.UpgrdLvl == 2)
                        {
                            spriteBatch.Draw(tow.Image, tow.Rect, Color.HotPink);
                        }
                    }
                    
                    
                }
                // draw the tower that's hanging off of the mouse
                if (tower is Hairbrush)
                {
                    Hairbrush hb = (Hairbrush)tower;
                    //hb.Col = Color.White;
                    hb.Draw(spriteBatch, gameTime, tower.RectX, tower.RectY);
                    
                }
                else
                spriteBatch.Draw(tower.Image, tower.Rect, tower.Clr);
            }

            // showing the towers you placed
            foreach (Weapon tow in towerList)
            {
                // still buggy, but checks to see if the tower on the mouse is intersecting with a tower that's already placed
                if (new Rectangle(mState.X, mState.Y, 1, 1).Intersects(new Rectangle(tow.RectX - 54, tow.RectY - 27, 81, 81)))
                {
                    tower.Clr = Color.Red;
                    if (tower is Hairbrush)
                    {
                        Hairbrush hb = (Hairbrush)tower;
                        hb.Col = Color.Red;
                    }
                }

                if (tow is Hairbrush)
                {
                    Hairbrush hb = (Hairbrush)tow;
                    hb.Col = Color.White;
                    hb.Draw(spriteBatch, gameTime, tow.RectX, tow.RectY);
                    try
                    {
                        foreach (Enemy bug in currentWave)
                        {
                            if (bug.IsFlying == false)
                            {
                                if (tow.RectX + 13 == bug.posX && tow.RectY + 13 == bug.posY)
                                {
                                    if (tow.Firing == true)
                                    {
                                        tow.Firing = false;
                                        hb.RunOver(gameTime, bug, bug.posX, bug.posY);
                                        if (hb.Bristles == 0)
                                        {
                                            towerList.Remove(hb);
                                            BuyingTowers();
                                            return;
                                        }
                                        break;
                                    }
                                }
                            }
                        }
                    }
                    catch { }

                    continue;
                }
                // drawing according to the tiles that were clicked
                if (tow.UpgrdLvl == 0)
                {
                    spriteBatch.Draw(tow.Image, tow.Rect, Color.White);
                }
                if (tow.UpgrdLvl == 1)
                {
                    spriteBatch.Draw(tow.Image, tow.Rect, Color.LightSalmon);
                }
                if (tow.UpgrdLvl == 2)
                {
                    spriteBatch.Draw(tow.Image, tow.Rect, Color.HotPink);
                }
            }

            foreach (Weapon tow in towerList)
            {
                // clicking on the tower to bring up the menu
                if (new Rectangle(mState.X, mState.Y, 1, 1).Intersects(tow.Rect) && mState.LeftButton == ButtonState.Pressed && prevMState.LeftButton == ButtonState.Released)
                {

                    if (tow is Lotion)
                    {
                        lMenu.Dr = true;
                        pMenu.Dr = false;
                        hMenu.Dr = false;
                        lSell.Col = Color.Green;
                        lU1.Col = Color.Green;
                        lU2.Col = Color.Green;

                        // drawing the menu
                        if (lMenu.Dr == true)
                        {
                            spriteBatch.Draw(lotionTowerMenuImage, new Vector2(0, 243), Color.White);
                            spriteBatch.DrawString(font, "200", new Vector2(105, 290), Color.Black);
                            spriteBatch.DrawString(font, "100", new Vector2(105, 354), Color.Black);
                            spriteBatch.DrawString(font, "150", new Vector2(15, 453), Color.Black);
                            spriteBatch.DrawString(font, "300", new Vector2(97, 453), Color.Black);

                            //buttons cont so they draw on top of the the menu screens
                            lBuy.Draw(spriteBatch);
                            lSell.Draw(spriteBatch);
                            lU1.Draw(spriteBatch);
                            lU2.Draw(spriteBatch);

                            if (lSell.Pressed == true && lSell.Col == Color.Green && tow.Sold != true)
                            {

                                player.Bubbles += 100;
                                try
                                {
                                    SoundEffectInstance soundInstance = bubbles.CreateInstance();
                                    soundInstance.Volume = 0.1f;
                                    soundInstance.Play();
                                }
                                catch { }
                                tow.Sold = true;
                                if (tow.UpgrdLvl == 1)
                                {
                                    upgradedLotionList.Remove(tow);
                                }
                                if (tow.UpgrdLvl == 2)
                                {
                                    upgradedLotionList2.Remove(tow);
                                }
                                towerList.Remove(tow);
                                lotionList.Remove(tow);                      
                                lMenu.Dr = false;
                                lBuy.Pressed = false;
                                lSell.Pressed = false;
                                break;
                            }
                            if(lU1.Pressed == true && lU1.Col == Color.Green && tow.UpgrdLvl == 0 && player.Bubbles >= 150)
                            {
                                player.Bubbles -= 150;
                                try
                                {
                                    SoundEffectInstance soundInstance = bubbles.CreateInstance();
                                    soundInstance.Volume = 0.1f;
                                    soundInstance.Play();
                                }
                                catch { }
                                tow.Upgrade();
                                upgradedLotionList.Add(tow);
                                lU1.Pressed = false;
                                lMenu.Dr = false;
                                break;
                            }
                            if (lU2.Pressed == true && lU2.Col == Color.Green && tow.UpgrdLvl == 1 && player.Bubbles >= 300)
                            {
                                player.Bubbles -= 300;
                                try
                                {
                                    SoundEffectInstance soundInstance = bubbles.CreateInstance();
                                    soundInstance.Volume = 0.1f;
                                    soundInstance.Play();
                                }
                                catch { }
                                tow.Upgrade();
                                upgradedLotionList.Remove(tow);
                                upgradedLotionList2.Add(tow);
                                lU2.Pressed = false;
                                lMenu.Dr = false;
                                break;
                            }
                        }

                    }
                    if (tow is PillShoot)
                    { 
                        pMenu.Dr = true;
                        lMenu.Dr = false;
                        hMenu.Dr = false;
                        lSell.Col = Color.Green;
                        lU1.Col = Color.Green;
                        lU2.Col = Color.Green;

                        // drawing the menu
                        if (pMenu.Dr == true)
                        {
                            spriteBatch.Draw(pillTowerMenuImage, new Vector2(0, 243), Color.White);
                            spriteBatch.DrawString(font, "200", new Vector2(105, 290), Color.Black);
                            spriteBatch.DrawString(font, "100", new Vector2(105, 354), Color.Black);
                            spriteBatch.DrawString(font, "150", new Vector2(15, 453), Color.Black);
                            spriteBatch.DrawString(font, "300", new Vector2(97, 453), Color.Black);

                            //buttons cont so they draw on top of the the menu screens
                            pBuy.Draw(spriteBatch);
                            pSell.Col = Color.Green;
                            pSell.Draw(spriteBatch);
                            pU1.Draw(spriteBatch);
                            pU2.Draw(spriteBatch);

                            if (pSell.Pressed == true && pSell.Col == Color.Green && tow.Sold != true)
                            {
                                pSell.Col = Color.Red;
                                player.Bubbles += 100;
                                try
                                {
                                    SoundEffectInstance soundInstance = bubbles.CreateInstance();
                                    soundInstance.Volume = 0.1f;
                                    soundInstance.Play();
                                }
                                catch { }
                                tow.Sold = true;
                                if (tow.UpgrdLvl == 1)
                                {
                                    upgradedPillShootList.Remove(tow);
                                }
                                if (tow.UpgrdLvl == 2)
                                {
                                    upgradedPillShootList2.Remove(tow);
                                }
                                towerList.Remove(tow);
                                pillShootList.Remove(tow);
                                pMenu.Dr = false;
                                pBuy.Pressed = false;
                                pSell.Pressed = false;
                                break;
                            }

                            if (pU1.Pressed == true && pU1.Col == Color.Green && tow.UpgrdLvl == 0 && player.Bubbles >= 150)
                            {
                                player.Bubbles -= 150;
                                try
                                {
                                    SoundEffectInstance soundInstance = bubbles.CreateInstance();
                                    soundInstance.Volume = 0.1f;
                                    soundInstance.Play();
                                }
                                catch { }
                                tow.Upgrade();
                                upgradedPillShootList.Add(tow);
                                pU1.Pressed = false;
                                pMenu.Dr = false;
                                break;
                            }
                            if (pU2.Pressed == true && pU2.Col == Color.Green && tow.UpgrdLvl == 1 && player.Bubbles >= 300)
                            {
                                player.Bubbles -= 300;
                                try
                                {
                                    SoundEffectInstance soundInstance = bubbles.CreateInstance();
                                    soundInstance.Volume = 0.1f;
                                    soundInstance.Play();
                                }
                                catch { }
                                tow.Upgrade();
                                upgradedPillShootList.Remove(tow);
                                upgradedPillShootList2.Remove(tow);
                                pU2.Pressed = false;
                                pMenu.Dr = false;
                                break;
                            }
                        }
                    }
                    if (tow is Hairbrush)
                    {
                        hMenu.Draw(spriteBatch);
                    }
                }
            }

            //buttons 
            lMenu.Draw(spriteBatch);
            pMenu.Draw(spriteBatch);
            hMenu.Draw(spriteBatch);


            //tower menus
            if (lMenu.Dr == true)
            {
                spriteBatch.Draw(lotionTowerMenuImage, new Vector2(0, 243), Color.White);
                spriteBatch.DrawString(font, "200", new Vector2(105, 290), Color.Black);
                spriteBatch.DrawString(font, "100", new Vector2(105, 354), Color.Black);
                spriteBatch.DrawString(font, "150", new Vector2(15, 453), Color.Black);
                spriteBatch.DrawString(font, "300", new Vector2(97, 453), Color.Black);
            }
            if (pMenu.Dr == true)
            {
                spriteBatch.Draw(pillTowerMenuImage, new Vector2(0, 243), Color.White);
                spriteBatch.DrawString(font, "200", new Vector2(105, 290), Color.Black);
                spriteBatch.DrawString(font, "100", new Vector2(105, 354), Color.Black);
                spriteBatch.DrawString(font, "150", new Vector2(15, 453), Color.Black);
                spriteBatch.DrawString(font, "300", new Vector2(97, 453), Color.Black);
            }
            if (hMenu.Dr == true)
            {
                spriteBatch.Draw(hairbrushTowerMenuImage, new Vector2(0, 243), Color.White);
                spriteBatch.DrawString(font, "150", new Vector2(105, 290), Color.Black);
                spriteBatch.DrawString(font, "N/A", new Vector2(110, 354), Color.Black);
                spriteBatch.DrawString(font, "N/A", new Vector2(20, 453), Color.Black);
                spriteBatch.DrawString(font, "N/A", new Vector2(97, 453), Color.Black);
            }


            //buttons cont so they draw on top of the the menu screens
            // if each respective list count is = 0, change the colors of the buttons (or if they simply can't afford it)
            if (player.Bubbles < 200)
            {
                lBuy.Col = Color.Red;
            }
            else
            {
                lBuy.Col = Color.Green;
            }
            lBuy.Draw(spriteBatch);
            if(lotionList.Count == 0)
            {
                lSell.Col = Color.Red;
            }
            else
            {
                lSell.Col = Color.Green;
            }
            lSell.Draw(spriteBatch);
            // if you have only maxed out lotion towers on the map, then you can't upgrade anything, so set the button to red
            if ((lotionList.Count == upgradedLotionList2.Count || lotionList.Count == upgradedLotionList.Count) || player.Bubbles < 150 || lBuy.Pressed == true)
            {
                lU1.Col = Color.Red;
            }
            else
            {
                lU1.Col = Color.Green;
            }
            lU1.Draw(spriteBatch);
            if (upgradedLotionList.Count == 0 || player.Bubbles < 300)
            {
                lU2.Col = Color.Red;
            }
            else
            {
                lU2.Col = Color.Green;
            }
            lU2.Draw(spriteBatch);
            if (player.Bubbles < 200)
            {
                pBuy.Col = Color.Red;
            }
            else
            {
                pBuy.Col = Color.Green;
            }
            pBuy.Draw(spriteBatch);
            if (pillShootList.Count == 0)
            {
                pSell.Col = Color.Red;
            }
            else
            {
                pSell.Col = Color.Green;
            }
            pSell.Draw(spriteBatch);
            // if you have only maxed out pill towers on the map, then you can't upgrade anything, so set the button to red
            if (pillShootList.Count == upgradedPillShootList2.Count || pillShootList.Count == upgradedPillShootList.Count || player.Bubbles < 150)
            {
                pU1.Col = Color.Red;
            }
            else
            {
                pU1.Col = Color.Green;
            }
            pU1.Draw(spriteBatch);
            if (upgradedPillShootList.Count == 0 || player.Bubbles < 300)
            {
                pU2.Col = Color.Red;
            }
            else
            {
                pU2.Col = Color.Green;
            }
            pU2.Draw(spriteBatch);
            if(player.Bubbles < 150)
            {
                hBuy.Col = Color.Red;
            }
            else
            {
                hBuy.Col = Color.Green;
            }
            hBuy.Draw(spriteBatch);
            hSell.Draw(spriteBatch);
            hU1.Draw(spriteBatch);
            hU2.Draw(spriteBatch);

        }

        protected void ReadFile()
        {
            // file location works for any computer with this project
            StreamReader exTool = new StreamReader("..\\..\\..\\..\\..\\..\\ExternalTool\\ExternalTool.txt");
            string attributes = exTool.ReadLine();
            Char delimiter = ',';
            String[] substrings = attributes.Split(delimiter); // split the words at the commas

            // ant health and parsing it
            string antHealthStr = substrings[0];
            int.TryParse(antHealthStr, out antHealth);
            // fly health and parse
            string flyHealthStr = substrings[1];
            int.TryParse(flyHealthStr, out flyHealth);
            // tarantula health and parse
            string tarHealthStr = substrings[2];
            int.TryParse(tarHealthStr, out tarHealth);
            // ant speed and parsing it
            string antSpeedStr = substrings[3];
            int.TryParse(antSpeedStr, out antSpeed);
            // fly speed and parsing it
            string flySpeedStr = substrings[4];
            int.TryParse(flySpeedStr, out flySpeed);
            // tarantula speed and parsing it
            string tarSpeedStr = substrings[5];
            int.TryParse(tarSpeedStr, out tarSpeed);
            // player health and parsing it
            string playerHealthStr = substrings[6];
            int.TryParse(playerHealthStr, out playerHealth);
            // player bubbles and parsing it
            string playerBubblesStr = substrings[7];
            int.TryParse(playerBubblesStr, out playerBubbles);
        }
    }
}