﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace ShowerDefence
{
    class Hairbrush : Weapon
    {
        //animation attributes 

        int frame;
        Point currentFrame;
        int numFrames;
        bool changeFrame;
        Point frameSize = new Point(54, 27);
        Rectangle rect;
        int bristles;
        Color col;
        Texture2D picture;

        enum Direction { Vert, Horis };
        Direction direction;

        // property
        public int Bristles
        {
            get { return bristles; }
            set { bristles = value; }
        }

        //constructor
        public Hairbrush(int cst, int dmg, int rng, int sell, double atkSp, bool airSh, Texture2D pic, Rectangle r, Color c) : base(cst, dmg, rng, sell, atkSp, airSh, pic, r, c)

        {
            bristles = 10;
            numFrames = 10;
            currentFrame = new Point(0, 0);
            picture = pic;
            frame = 1;
            direction = Direction.Horis;
            col = c;
            Firing = true;
        }
        //public Rectangle Rect
        //{
        //   get { return rect; }
        //}

        //change the frame 
        public void RunOver(GameTime gameTime, Enemy bug, int x, int y)
        {
            bug.Health -= 20;
            if (bug.Health <= 0)
            {
                bug.Rect = new Rectangle(-100000, -100000, 1, 1);
                bug.Die();
            }
            changeFrame = true;
            Update(gameTime, x, y);
            bristles--;
            return;
        }

        public bool ChangeFrame
        {
            get { return changeFrame; }
            set { changeFrame = value; }
        }
        public Color Col
        {
            get { return col; }
            set { col = value; }
        }

        public void Update(GameTime gameTime, int x, int y)
        {
            // if it's on path 1
            if (x / 27 == 7 && y / 27 >= 0 && y / 27 <= 18)
            {
                direction = Direction.Vert;
            }
            // path 2
            else if (y / 27 == 18 && x / 27 >= 7 && x / 27 <= 22)
            {
                direction = Direction.Horis;
            }
            // path 3
            else if (x / 27 == 23 && y / 27 <= 19 && y / 27 >= 14)
            {
                direction = Direction.Vert;
            }
            // path 4
            else if (y / 27 == 13 && x / 27 >= 10 && x / 27 <= 24)
            {
                direction = Direction.Horis;
            }
            // path 5
            else if (x / 27 == 10 && y / 27 <= 13 && y / 27 >= 5)
            {
                direction = Direction.Vert;
            }
            // path 6
            else if (y / 27 > 3 && y / 27 < 5 && (x / 27 >= 8 && x / 27 <= 15))
            {
                direction = Direction.Horis;
            }
            // path 7
            else if (x / 27 > 14 && x / 27 < 16 && (y / 27 >= 4 && y / 27 <= 10))
            {
                direction = Direction.Vert;
            }
            // path 8
            else if (y / 27 > 9 && y / 27 < 11 && (x / 27 >= 15 && x / 27 <= 17))
            {
                direction = Direction.Horis;
            }
            // path 9
            else if (x / 27 > 17 && x / 27 < 19 && (y / 27 >= 5 && y / 27 <= 10))
            {
                direction = Direction.Vert;
            }
            // path 10
            else if (y / 27 > 3 && y / 27 < 5 && (x / 27 >= 18 && x / 27 <= 23))
            {
                direction = Direction.Horis;
            }
            // path 11
            else if (x / 27 > 22 && x / 27 < 24 && (y / 27 >= 4 && y / 27 <= 10))
            {
                direction = Direction.Vert;
            }
            // path 12
            else if (y / 27 > 11 && (x / 27 >= 24 && x / 27 <= 27))
            {
                direction = Direction.Horis;
            }
            // otherwise make horizontal
            else
            {
                direction = Direction.Horis;
            }
            if (changeFrame == true)
            {
                frame++;
                //loop frames
                //if (frame > numFrames)
               // {
               //     frame = 1; //restart
                //}

                //switch statement for frames
                switch (frame)
                {
                    case 1:
                        currentFrame.X = 0;
                        currentFrame.Y = 0;
                        changeFrame = false;
                        break;
                    case 2:
                        currentFrame.X = 55;
                        currentFrame.Y = 0;
                        changeFrame = false;
                        break;
                    case 3:
                        currentFrame.X = 0;
                        currentFrame.Y = 28;
                        changeFrame = false;
                        break;
                    case 4:
                        currentFrame.X = 55;
                        currentFrame.Y = 28;
                        changeFrame = false;
                        break;
                    case 5:
                        currentFrame.X = 0;
                        currentFrame.Y = 55;
                        changeFrame = false;
                        break;
                    case 6:
                        currentFrame.X = 55;
                        currentFrame.Y = 55;
                        changeFrame = false;
                        break;
                    case 7:
                        currentFrame.X = 0;
                        currentFrame.Y = 82;
                        changeFrame = false;
                        break;
                    case 8:
                        currentFrame.X = 55;
                        currentFrame.Y = 82;
                        changeFrame = false;
                        break;
                    case 9:
                        currentFrame.X = 0;
                        currentFrame.Y = 109;
                        changeFrame = false;
                        break;
                    case 10:
                        currentFrame.X = 55;
                        currentFrame.Y = 109;
                        changeFrame = false;
                        break;
                }
            }
        }
        public void Draw(SpriteBatch spriteBatch, GameTime gameTime, int x, int y)
        {
            rect = new Rectangle(currentFrame.X, currentFrame.Y , frameSize.X, frameSize.Y);
            if (bristles > 0)
            {
                if (direction == Direction.Vert)
                {
                    spriteBatch.Draw(picture, new Vector2(x, y), rect, Col, -1.59f, new Vector2(this.frameSize.X / 2, this.frameSize.Y / 2 - 27 / 2), 1, SpriteEffects.None, 0);
                }
                else if (direction == Direction.Horis)
                {
                    spriteBatch.Draw(picture, new Vector2(x, y), rect, Col);
                }
            }
        }
    }
}
