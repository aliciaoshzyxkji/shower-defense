﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace ShowerDefence
{
    class PillShoot : Weapon
    {
        public PillShoot(int cst, int dmg, int rng, int sell, double atkSp, bool airSh, Texture2D pic, Rectangle r, Color c, Texture2D bulImg) : base(cst, dmg, rng, sell, atkSp, airSh, pic, r, c, bulImg)
        {
        }

        public override void Upgrade()
        {
            base.Upgrade();
            if (UpgrdLvl == 1)
            {
                Damage *= 2;
                Range += 1;
            }
            if (UpgrdLvl == 2)
            {
                Damage *= 2;
                Range += 1;
            }
        }
    }
}
