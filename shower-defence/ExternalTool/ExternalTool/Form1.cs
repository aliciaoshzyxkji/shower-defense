﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace ExternalTool
{
    public partial class Form1 : Form
    {
        // attributes
        string[] attributes = new string[8];
        // what will be the full string we write to the file
        string attributesStr = "";
        // seeing if that bar ever moved or not
        bool antHealthMoved = false;
        bool flyHealthMoved = false;
        bool tarHealthMoved = false;

        bool antSpeedMoved = false;
        bool flySpeedMoved = false;
        bool tarSpeedMoved = false;

        bool playerHealthMoved = false;
        bool playerBubblesMoved = false;

        public Form1()
        {
            InitializeComponent();
        }

        private void antHealthBar_Scroll(object sender, EventArgs e)
        {
            if (antHealthBar.Value == 0)
            {
                attributes[0] = "5,";
                antHealthMoved = true;
            }
            if (antHealthBar.Value == 1)
            {
                attributes[0] = "10,";
                antHealthMoved = true;
            }
            if (antHealthBar.Value == 2)
            {
                attributes[0] = "15,";
                antHealthMoved = true;
            }
            if (antHealthBar.Value == 3)
            {
                attributes[0] = "20,";
                antHealthMoved = true;
            }
            if (antHealthBar.Value == 4) // setting all the values based on the bar's location
            {
                attributes[0] = "25,";
                antHealthMoved = true;
            }
            if (antHealthBar.Value == 5)
            {
                attributes[0] = "30,";
                antHealthMoved = true;
            }
            if (antHealthBar.Value == 6)
            {
                attributes[0] = "35,";
                antHealthMoved = true;
            }
            if (antHealthBar.Value == 7)
            {
                attributes[0] = "40,";
                antHealthMoved = true;
            }
            if (antHealthBar.Value == 8)
            {
                attributes[0] = "45,";
                antHealthMoved = true;
            }
        }

        private void flyHealthBar_Scroll(object sender, EventArgs e)
        {
            if (flyHealthBar.Value == 0)
            {
                attributes[1] = "3,";
                flyHealthMoved = true;
            }
            if (flyHealthBar.Value == 1)
            {
                attributes[1] = "6,";
                flyHealthMoved = true;
            }
            if (flyHealthBar.Value == 2)
            {
                attributes[1] = "9,";
                flyHealthMoved = true;
            }
            if (flyHealthBar.Value == 3)
            {
                attributes[1] = "12,";
                flyHealthMoved = true;
            }
            if (flyHealthBar.Value == 4) // setting all the values based on the bar's location
            {
                attributes[1] = "15,";
                flyHealthMoved = true;
            }
            if (flyHealthBar.Value == 5)
            {
                attributes[1] = "18,";
                flyHealthMoved = true;
            }
            if (flyHealthBar.Value == 6)
            {
                attributes[1] = "21,";
                flyHealthMoved = true;
            }
            if (flyHealthBar.Value == 7)
            {
                attributes[1] = "24,";
                flyHealthMoved = true;
            }
            if (flyHealthBar.Value == 8)
            {
                attributes[1] = "27,";
                flyHealthMoved = true;
            }
        }

        private void tarantulaHealthBar_Scroll(object sender, EventArgs e)
        {
            if (tarantulaHealthBar.Value == 0)
            {
                attributes[2] = "50,";
                tarHealthMoved = true;
            }
            if (tarantulaHealthBar.Value == 1)
            {
                attributes[2] = "75,";
                tarHealthMoved = true;
            }
            if (tarantulaHealthBar.Value == 2)
            {
                attributes[2] = "100,";
                tarHealthMoved = true;
            }
            if (tarantulaHealthBar.Value == 3)
            {
                attributes[2] = "125,";
                tarHealthMoved = true;
            }
            if (tarantulaHealthBar.Value == 4) // setting all the values based on the bar's location
            {
                attributes[2] = "150,";
                tarHealthMoved = true;
            }
            if (tarantulaHealthBar.Value == 5)
            {
                attributes[2] = "175,";
                tarHealthMoved = true;
            }
            if (tarantulaHealthBar.Value == 6)
            {
                attributes[2] = "200,";
                tarHealthMoved = true;
            }
            if (tarantulaHealthBar.Value == 7)
            {
                attributes[2] = "225,";
                tarHealthMoved = true;
            }
            if (tarantulaHealthBar.Value == 8)
            {
                attributes[2] = "250,";
                tarHealthMoved = true;
            }
        }

        private void antSpeedBar_Scroll(object sender, EventArgs e)
        {
            if (antSpeedBar.Value == 0)
            {
                attributes[3] = "4,";
                antSpeedMoved = true;
            }
            if (antSpeedBar.Value == 1)
            {
                attributes[3] = "8,";
                antSpeedMoved = true;
            }
            if (antSpeedBar.Value == 2)
            {
                attributes[3] = "12,";
                antSpeedMoved = true;
            }
            if (antSpeedBar.Value == 3)
            {
                attributes[3] = "16,";
                antSpeedMoved = true;
            }
            if (antSpeedBar.Value == 4) // setting the values for speed of this enemy
            {
                attributes[3] = "20,";
                antSpeedMoved = true;
            }
            if (antSpeedBar.Value == 5)
            {
                attributes[3] = "24,";
                antSpeedMoved = true;
            }
            if (antSpeedBar.Value == 6)
            {
                attributes[3] = "28,";
                antSpeedMoved = true;
            }
            if (antSpeedBar.Value == 7)
            {
                attributes[3] = "32,";
                antSpeedMoved = true;
            }
            if (antSpeedBar.Value == 8)
            {
                attributes[3] = "36,";
                antSpeedMoved = true;
            }
        }

        private void flySpeedBar_Scroll(object sender, EventArgs e)
        {
            if (flySpeedBar.Value == 0)
            {
                attributes[4] = "10,";
                flySpeedMoved = true;
            }
            if (flySpeedBar.Value == 1)
            {
                attributes[4] = "20,";
                flySpeedMoved = true;
            }
            if (flySpeedBar.Value == 2)
            {
                attributes[4] = "30,";
                flySpeedMoved = true;
            }
            if (flySpeedBar.Value == 3)
            {
                attributes[4] = "40,";
                flySpeedMoved = true;
            }
            if (flySpeedBar.Value == 4) // setting the values for speed of this enemy
            {
                attributes[4] = "50,";
                flySpeedMoved = true;
            }
            if (flySpeedBar.Value == 5)
            {
                attributes[4] = "60,";
                flySpeedMoved = true;
            }
            if (flySpeedBar.Value == 6)
            {
                attributes[4] = "70,";
                flySpeedMoved = true;
            }
            if (flySpeedBar.Value == 7)
            {
                attributes[4] = "80,";
                flySpeedMoved = true;
            }
            if (flySpeedBar.Value == 8)
            {
                attributes[4] = "90,";
                flySpeedMoved = true;
            }
        }

        private void tarantulaSpeedBar_Scroll(object sender, EventArgs e)
        {
            if (tarantulaSpeedBar.Value == 0)
            {
                attributes[5] = "2,";
                tarSpeedMoved = true;
            }
            if (tarantulaSpeedBar.Value == 1)
            {
                attributes[5] = "4,";
                tarSpeedMoved = true;
            }
            if (tarantulaSpeedBar.Value == 2)
            {
                attributes[5] = "6,";
                tarSpeedMoved = true;
            }
            if (tarantulaSpeedBar.Value == 3)
            {
                attributes[5] = "8,";
                tarSpeedMoved = true;
            }
            if (tarantulaSpeedBar.Value == 4) // setting the values for speed of this enemy
            {
                attributes[5] = "10,";
                tarSpeedMoved = true;
            }
            if (tarantulaSpeedBar.Value == 5)
            {
                attributes[5] = "12,";
                tarSpeedMoved = true;
            }
            if (tarantulaSpeedBar.Value == 6)
            {
                attributes[5] = "14,";
                tarSpeedMoved = true;
            }
            if (tarantulaSpeedBar.Value == 7)
            {
                attributes[5] = "16,";
                tarSpeedMoved = true;
            }
            if (tarantulaSpeedBar.Value == 8)
            {
                attributes[5] = "18,";
                tarSpeedMoved = true;
            }
        }

        private void playerHealthBar_Scroll(object sender, EventArgs e)
        {
            if (playerHealthBar.Value == 0)
            {
                attributes[6] = "2,";
                playerHealthMoved = true;
            }
            if (playerHealthBar.Value == 1)
            {
                attributes[6] = "4,";
                playerHealthMoved = true;
            }
            if (playerHealthBar.Value == 2)
            {
                attributes[6] = "6,";
                playerHealthMoved = true;
            }
            if (playerHealthBar.Value == 3)
            {
                attributes[6] = "8,";
                playerHealthMoved = true;
            }
            if (playerHealthBar.Value == 4) // setting the values for the starting player health value
            {
                attributes[6] = "10,";
                playerHealthMoved = true;
            }
            if (playerHealthBar.Value == 5)
            {
                attributes[6] = "12,";
                playerHealthMoved = true;
            }
            if (playerHealthBar.Value == 6)
            {
                attributes[6] = "14,";
                playerHealthMoved = true;
            }
            if (playerHealthBar.Value == 7)
            {
                attributes[6] = "16,";
                playerHealthMoved = true;
            }
            if (playerHealthBar.Value == 8)
            {
                attributes[6] = "18,";
                playerHealthMoved = true;
            }
        }

        private void playerBubblesBar_Scroll(object sender, EventArgs e)
        {
            if (playerBubblesBar.Value == 0)
            {
                attributes[7] = "100,";
                playerBubblesMoved = true;
            }
            if (playerBubblesBar.Value == 1)
            {
                attributes[7] = "200,";
                playerBubblesMoved = true;
            }
            if (playerBubblesBar.Value == 2)
            {
                attributes[7] = "300,";
                playerBubblesMoved = true;
            }
            if (playerBubblesBar.Value == 3)
            {
                attributes[7] = "400,";
                playerBubblesMoved = true;
            }
            if (playerBubblesBar.Value == 4) // setting the values for the starting player health value
            {
                attributes[7] = "500,";
                playerBubblesMoved = true;
            }
            if (playerBubblesBar.Value == 5)
            {
                attributes[7] = "600,";
                playerBubblesMoved = true;
            }
            if (playerBubblesBar.Value == 6)
            {
                attributes[7] = "700,";
                playerBubblesMoved = true;
            }
            if (playerBubblesBar.Value == 7)
            {
                attributes[7] = "800,";
                playerBubblesMoved = true;
            }
            if (playerBubblesBar.Value == 8)
            {
                attributes[7] = "900,";
                playerBubblesMoved = true;
            }
        }

        private void saveButton_Click(object sender, EventArgs e)
        {
            if (antHealthMoved == false)
            {
                attributes[0] = "25,";
            }
            if (flyHealthMoved == false)
            {
                attributes[1] = "15,"; // these are so if the player doesn't move the bar at all but hits the save option
            }                         // need to still save the default values
            if (tarHealthMoved == false)
            {
                attributes[2] = "150,";
            }
            if(antSpeedMoved == false)
            {
                attributes[3] = "20,";
            }
            if (flySpeedMoved == false)
            {
                attributes[4] = "50,";
            }
            if (tarSpeedMoved == false)
            {
                attributes[5] = "10,";
            }
            if (playerHealthMoved == false)
            {
                attributes[6] = "10,";
            }
            if (playerBubblesMoved == false)
            {
                attributes[7] = "500,";
            }

            StreamWriter exTool = new StreamWriter("..\\..\\..\\ExternalTool.txt"); // save to the top folder where the exe is
            foreach (string atr in attributes)
            {
                attributesStr += atr; // add all the arrays we had from the strings
            }
            exTool.WriteLine(attributesStr); // write to them
            exTool.Close();
            attributesStr = ""; // clear the string
        }

        private void quitButton_Click(object sender, EventArgs e)
        {
            Application.Exit(); // close the program when quit is pressed
        }
    }
}
