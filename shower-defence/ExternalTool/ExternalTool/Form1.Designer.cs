﻿namespace ExternalTool
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.antHealthBar = new System.Windows.Forms.TrackBar();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.flyHealthBar = new System.Windows.Forms.TrackBar();
            this.label23 = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.label26 = new System.Windows.Forms.Label();
            this.label27 = new System.Windows.Forms.Label();
            this.label28 = new System.Windows.Forms.Label();
            this.label29 = new System.Windows.Forms.Label();
            this.label30 = new System.Windows.Forms.Label();
            this.label31 = new System.Windows.Forms.Label();
            this.label32 = new System.Windows.Forms.Label();
            this.label33 = new System.Windows.Forms.Label();
            this.tarantulaHealthBar = new System.Windows.Forms.TrackBar();
            this.quitButton = new System.Windows.Forms.Button();
            this.saveButton = new System.Windows.Forms.Button();
            this.label10 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.label34 = new System.Windows.Forms.Label();
            this.label35 = new System.Windows.Forms.Label();
            this.label36 = new System.Windows.Forms.Label();
            this.label37 = new System.Windows.Forms.Label();
            this.label38 = new System.Windows.Forms.Label();
            this.label39 = new System.Windows.Forms.Label();
            this.label40 = new System.Windows.Forms.Label();
            this.antSpeedBar = new System.Windows.Forms.TrackBar();
            this.label41 = new System.Windows.Forms.Label();
            this.label42 = new System.Windows.Forms.Label();
            this.label43 = new System.Windows.Forms.Label();
            this.label44 = new System.Windows.Forms.Label();
            this.label45 = new System.Windows.Forms.Label();
            this.label46 = new System.Windows.Forms.Label();
            this.label47 = new System.Windows.Forms.Label();
            this.label48 = new System.Windows.Forms.Label();
            this.label49 = new System.Windows.Forms.Label();
            this.label50 = new System.Windows.Forms.Label();
            this.flySpeedBar = new System.Windows.Forms.TrackBar();
            this.label51 = new System.Windows.Forms.Label();
            this.label52 = new System.Windows.Forms.Label();
            this.label53 = new System.Windows.Forms.Label();
            this.label54 = new System.Windows.Forms.Label();
            this.label55 = new System.Windows.Forms.Label();
            this.label56 = new System.Windows.Forms.Label();
            this.label57 = new System.Windows.Forms.Label();
            this.label58 = new System.Windows.Forms.Label();
            this.label59 = new System.Windows.Forms.Label();
            this.label60 = new System.Windows.Forms.Label();
            this.tarantulaSpeedBar = new System.Windows.Forms.TrackBar();
            this.label61 = new System.Windows.Forms.Label();
            this.label62 = new System.Windows.Forms.Label();
            this.label63 = new System.Windows.Forms.Label();
            this.label64 = new System.Windows.Forms.Label();
            this.label65 = new System.Windows.Forms.Label();
            this.label66 = new System.Windows.Forms.Label();
            this.label67 = new System.Windows.Forms.Label();
            this.label68 = new System.Windows.Forms.Label();
            this.label69 = new System.Windows.Forms.Label();
            this.label70 = new System.Windows.Forms.Label();
            this.playerHealthBar = new System.Windows.Forms.TrackBar();
            this.label71 = new System.Windows.Forms.Label();
            this.label72 = new System.Windows.Forms.Label();
            this.label73 = new System.Windows.Forms.Label();
            this.label74 = new System.Windows.Forms.Label();
            this.label75 = new System.Windows.Forms.Label();
            this.label76 = new System.Windows.Forms.Label();
            this.label77 = new System.Windows.Forms.Label();
            this.label78 = new System.Windows.Forms.Label();
            this.label79 = new System.Windows.Forms.Label();
            this.label80 = new System.Windows.Forms.Label();
            this.playerBubblesBar = new System.Windows.Forms.TrackBar();
            ((System.ComponentModel.ISupportInitialize)(this.antHealthBar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.flyHealthBar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tarantulaHealthBar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.antSpeedBar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.flySpeedBar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tarantulaSpeedBar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.playerHealthBar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.playerBubblesBar)).BeginInit();
            this.SuspendLayout();
            // 
            // antHealthBar
            // 
            this.antHealthBar.Location = new System.Drawing.Point(27, 69);
            this.antHealthBar.Maximum = 8;
            this.antHealthBar.Name = "antHealthBar";
            this.antHealthBar.Size = new System.Drawing.Size(374, 45);
            this.antHealthBar.TabIndex = 0;
            this.antHealthBar.Value = 4;
            this.antHealthBar.Scroll += new System.EventHandler(this.antHealthBar_Scroll);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(35, 117);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(13, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "5";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(77, 117);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(19, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "10";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(119, 117);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(19, 13);
            this.label3.TabIndex = 3;
            this.label3.Text = "15";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(163, 117);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(19, 13);
            this.label4.TabIndex = 4;
            this.label4.Text = "20";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(205, 117);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(19, 13);
            this.label5.TabIndex = 5;
            this.label5.Text = "25";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(250, 117);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(19, 13);
            this.label6.TabIndex = 6;
            this.label6.Text = "30";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(291, 117);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(19, 13);
            this.label7.TabIndex = 7;
            this.label7.Text = "35";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(335, 117);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(19, 13);
            this.label8.TabIndex = 8;
            this.label8.Text = "40";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(378, 117);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(19, 13);
            this.label9.TabIndex = 9;
            this.label9.Text = "45";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(143, 35);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(151, 31);
            this.label12.TabIndex = 12;
            this.label12.Text = "Ant Health";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(147, 175);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(147, 31);
            this.label11.TabIndex = 24;
            this.label11.Text = "Fly Health";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(378, 257);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(19, 13);
            this.label14.TabIndex = 22;
            this.label14.Text = "27";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(335, 257);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(19, 13);
            this.label15.TabIndex = 21;
            this.label15.Text = "24";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(291, 257);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(19, 13);
            this.label16.TabIndex = 20;
            this.label16.Text = "21";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(247, 257);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(19, 13);
            this.label17.TabIndex = 19;
            this.label17.Text = "18";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(205, 257);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(19, 13);
            this.label18.TabIndex = 18;
            this.label18.Text = "15";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(161, 257);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(19, 13);
            this.label19.TabIndex = 17;
            this.label19.Text = "12";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(119, 257);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(13, 13);
            this.label20.TabIndex = 16;
            this.label20.Text = "9";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(76, 257);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(13, 13);
            this.label21.TabIndex = 15;
            this.label21.Text = "6";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(35, 257);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(13, 13);
            this.label22.TabIndex = 14;
            this.label22.Text = "3";
            // 
            // flyHealthBar
            // 
            this.flyHealthBar.Location = new System.Drawing.Point(27, 209);
            this.flyHealthBar.Maximum = 8;
            this.flyHealthBar.Name = "flyHealthBar";
            this.flyHealthBar.Size = new System.Drawing.Size(374, 45);
            this.flyHealthBar.TabIndex = 13;
            this.flyHealthBar.Value = 4;
            this.flyHealthBar.Scroll += new System.EventHandler(this.flyHealthBar_Scroll);
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label23.Location = new System.Drawing.Point(109, 318);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(231, 31);
            this.label23.TabIndex = 36;
            this.label23.Text = "Tarantula Health";
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Location = new System.Drawing.Point(376, 400);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(25, 13);
            this.label25.TabIndex = 34;
            this.label25.Text = "250";
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Location = new System.Drawing.Point(332, 400);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(25, 13);
            this.label26.TabIndex = 33;
            this.label26.Text = "225";
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Location = new System.Drawing.Point(287, 400);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(25, 13);
            this.label27.TabIndex = 32;
            this.label27.Text = "200";
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Location = new System.Drawing.Point(247, 400);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(25, 13);
            this.label28.TabIndex = 31;
            this.label28.Text = "175";
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Location = new System.Drawing.Point(203, 400);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(25, 13);
            this.label29.TabIndex = 30;
            this.label29.Text = "150";
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Location = new System.Drawing.Point(159, 400);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(25, 13);
            this.label30.TabIndex = 29;
            this.label30.Text = "125";
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Location = new System.Drawing.Point(112, 400);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(25, 13);
            this.label31.TabIndex = 28;
            this.label31.Text = "100";
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Location = new System.Drawing.Point(70, 400);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(19, 13);
            this.label32.TabIndex = 27;
            this.label32.Text = "75";
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Location = new System.Drawing.Point(35, 400);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(19, 13);
            this.label33.TabIndex = 26;
            this.label33.Text = "50";
            // 
            // tarantulaHealthBar
            // 
            this.tarantulaHealthBar.Location = new System.Drawing.Point(27, 352);
            this.tarantulaHealthBar.Maximum = 8;
            this.tarantulaHealthBar.Name = "tarantulaHealthBar";
            this.tarantulaHealthBar.Size = new System.Drawing.Size(374, 45);
            this.tarantulaHealthBar.TabIndex = 25;
            this.tarantulaHealthBar.Value = 4;
            this.tarantulaHealthBar.Scroll += new System.EventHandler(this.tarantulaHealthBar_Scroll);
            // 
            // quitButton
            // 
            this.quitButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.quitButton.Location = new System.Drawing.Point(12, 524);
            this.quitButton.Name = "quitButton";
            this.quitButton.Size = new System.Drawing.Size(132, 50);
            this.quitButton.TabIndex = 38;
            this.quitButton.Text = "QUIT";
            this.quitButton.UseVisualStyleBackColor = true;
            this.quitButton.Click += new System.EventHandler(this.quitButton_Click);
            // 
            // saveButton
            // 
            this.saveButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.saveButton.Location = new System.Drawing.Point(1091, 524);
            this.saveButton.Name = "saveButton";
            this.saveButton.Size = new System.Drawing.Size(132, 50);
            this.saveButton.TabIndex = 39;
            this.saveButton.Text = "SAVE";
            this.saveButton.UseVisualStyleBackColor = true;
            this.saveButton.Click += new System.EventHandler(this.saveButton_Click);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(547, 35);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(149, 31);
            this.label10.TabIndex = 50;
            this.label10.Text = "Ant Speed";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(782, 117);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(19, 13);
            this.label13.TabIndex = 49;
            this.label13.Text = "36";
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Location = new System.Drawing.Point(739, 117);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(19, 13);
            this.label24.TabIndex = 48;
            this.label24.Text = "32";
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.Location = new System.Drawing.Point(695, 117);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(19, 13);
            this.label34.TabIndex = 47;
            this.label34.Text = "28";
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.Location = new System.Drawing.Point(654, 117);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(19, 13);
            this.label35.TabIndex = 46;
            this.label35.Text = "24";
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.Location = new System.Drawing.Point(609, 117);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(19, 13);
            this.label36.TabIndex = 45;
            this.label36.Text = "20";
            // 
            // label37
            // 
            this.label37.AutoSize = true;
            this.label37.Location = new System.Drawing.Point(567, 117);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(19, 13);
            this.label37.TabIndex = 44;
            this.label37.Text = "16";
            // 
            // label38
            // 
            this.label38.AutoSize = true;
            this.label38.Location = new System.Drawing.Point(523, 117);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(19, 13);
            this.label38.TabIndex = 43;
            this.label38.Text = "12";
            // 
            // label39
            // 
            this.label39.AutoSize = true;
            this.label39.Location = new System.Drawing.Point(481, 117);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(13, 13);
            this.label39.TabIndex = 42;
            this.label39.Text = "8";
            // 
            // label40
            // 
            this.label40.AutoSize = true;
            this.label40.Location = new System.Drawing.Point(439, 117);
            this.label40.Name = "label40";
            this.label40.Size = new System.Drawing.Size(13, 13);
            this.label40.TabIndex = 41;
            this.label40.Text = "4";
            // 
            // antSpeedBar
            // 
            this.antSpeedBar.Location = new System.Drawing.Point(431, 69);
            this.antSpeedBar.Maximum = 8;
            this.antSpeedBar.Name = "antSpeedBar";
            this.antSpeedBar.Size = new System.Drawing.Size(374, 45);
            this.antSpeedBar.TabIndex = 40;
            this.antSpeedBar.Value = 4;
            this.antSpeedBar.Scroll += new System.EventHandler(this.antSpeedBar_Scroll);
            // 
            // label41
            // 
            this.label41.AutoSize = true;
            this.label41.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label41.Location = new System.Drawing.Point(547, 175);
            this.label41.Name = "label41";
            this.label41.Size = new System.Drawing.Size(145, 31);
            this.label41.TabIndex = 61;
            this.label41.Text = "Fly Speed";
            // 
            // label42
            // 
            this.label42.AutoSize = true;
            this.label42.Location = new System.Drawing.Point(782, 257);
            this.label42.Name = "label42";
            this.label42.Size = new System.Drawing.Size(19, 13);
            this.label42.TabIndex = 60;
            this.label42.Text = "90";
            // 
            // label43
            // 
            this.label43.AutoSize = true;
            this.label43.Location = new System.Drawing.Point(739, 257);
            this.label43.Name = "label43";
            this.label43.Size = new System.Drawing.Size(19, 13);
            this.label43.TabIndex = 59;
            this.label43.Text = "80";
            // 
            // label44
            // 
            this.label44.AutoSize = true;
            this.label44.Location = new System.Drawing.Point(695, 257);
            this.label44.Name = "label44";
            this.label44.Size = new System.Drawing.Size(19, 13);
            this.label44.TabIndex = 58;
            this.label44.Text = "70";
            // 
            // label45
            // 
            this.label45.AutoSize = true;
            this.label45.Location = new System.Drawing.Point(654, 257);
            this.label45.Name = "label45";
            this.label45.Size = new System.Drawing.Size(19, 13);
            this.label45.TabIndex = 57;
            this.label45.Text = "60";
            // 
            // label46
            // 
            this.label46.AutoSize = true;
            this.label46.Location = new System.Drawing.Point(609, 257);
            this.label46.Name = "label46";
            this.label46.Size = new System.Drawing.Size(19, 13);
            this.label46.TabIndex = 56;
            this.label46.Text = "50";
            // 
            // label47
            // 
            this.label47.AutoSize = true;
            this.label47.Location = new System.Drawing.Point(567, 257);
            this.label47.Name = "label47";
            this.label47.Size = new System.Drawing.Size(19, 13);
            this.label47.TabIndex = 55;
            this.label47.Text = "40";
            // 
            // label48
            // 
            this.label48.AutoSize = true;
            this.label48.Location = new System.Drawing.Point(523, 257);
            this.label48.Name = "label48";
            this.label48.Size = new System.Drawing.Size(19, 13);
            this.label48.TabIndex = 54;
            this.label48.Text = "30";
            // 
            // label49
            // 
            this.label49.AutoSize = true;
            this.label49.Location = new System.Drawing.Point(481, 257);
            this.label49.Name = "label49";
            this.label49.Size = new System.Drawing.Size(19, 13);
            this.label49.TabIndex = 53;
            this.label49.Text = "20";
            // 
            // label50
            // 
            this.label50.AutoSize = true;
            this.label50.Location = new System.Drawing.Point(439, 257);
            this.label50.Name = "label50";
            this.label50.Size = new System.Drawing.Size(19, 13);
            this.label50.TabIndex = 52;
            this.label50.Text = "10";
            // 
            // flySpeedBar
            // 
            this.flySpeedBar.Location = new System.Drawing.Point(431, 209);
            this.flySpeedBar.Maximum = 8;
            this.flySpeedBar.Name = "flySpeedBar";
            this.flySpeedBar.Size = new System.Drawing.Size(374, 45);
            this.flySpeedBar.TabIndex = 51;
            this.flySpeedBar.Value = 4;
            this.flySpeedBar.Scroll += new System.EventHandler(this.flySpeedBar_Scroll);
            // 
            // label51
            // 
            this.label51.AutoSize = true;
            this.label51.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label51.Location = new System.Drawing.Point(506, 318);
            this.label51.Name = "label51";
            this.label51.Size = new System.Drawing.Size(229, 31);
            this.label51.TabIndex = 72;
            this.label51.Text = "Tarantula Speed";
            // 
            // label52
            // 
            this.label52.AutoSize = true;
            this.label52.Location = new System.Drawing.Point(782, 400);
            this.label52.Name = "label52";
            this.label52.Size = new System.Drawing.Size(19, 13);
            this.label52.TabIndex = 71;
            this.label52.Text = "18";
            // 
            // label53
            // 
            this.label53.AutoSize = true;
            this.label53.Location = new System.Drawing.Point(739, 400);
            this.label53.Name = "label53";
            this.label53.Size = new System.Drawing.Size(19, 13);
            this.label53.TabIndex = 70;
            this.label53.Text = "16";
            // 
            // label54
            // 
            this.label54.AutoSize = true;
            this.label54.Location = new System.Drawing.Point(695, 400);
            this.label54.Name = "label54";
            this.label54.Size = new System.Drawing.Size(19, 13);
            this.label54.TabIndex = 69;
            this.label54.Text = "14";
            // 
            // label55
            // 
            this.label55.AutoSize = true;
            this.label55.Location = new System.Drawing.Point(654, 400);
            this.label55.Name = "label55";
            this.label55.Size = new System.Drawing.Size(19, 13);
            this.label55.TabIndex = 68;
            this.label55.Text = "12";
            // 
            // label56
            // 
            this.label56.AutoSize = true;
            this.label56.Location = new System.Drawing.Point(609, 400);
            this.label56.Name = "label56";
            this.label56.Size = new System.Drawing.Size(19, 13);
            this.label56.TabIndex = 67;
            this.label56.Text = "10";
            // 
            // label57
            // 
            this.label57.AutoSize = true;
            this.label57.Location = new System.Drawing.Point(567, 400);
            this.label57.Name = "label57";
            this.label57.Size = new System.Drawing.Size(13, 13);
            this.label57.TabIndex = 66;
            this.label57.Text = "8";
            // 
            // label58
            // 
            this.label58.AutoSize = true;
            this.label58.Location = new System.Drawing.Point(523, 400);
            this.label58.Name = "label58";
            this.label58.Size = new System.Drawing.Size(13, 13);
            this.label58.TabIndex = 65;
            this.label58.Text = "6";
            // 
            // label59
            // 
            this.label59.AutoSize = true;
            this.label59.Location = new System.Drawing.Point(481, 400);
            this.label59.Name = "label59";
            this.label59.Size = new System.Drawing.Size(13, 13);
            this.label59.TabIndex = 64;
            this.label59.Text = "4";
            // 
            // label60
            // 
            this.label60.AutoSize = true;
            this.label60.Location = new System.Drawing.Point(439, 400);
            this.label60.Name = "label60";
            this.label60.Size = new System.Drawing.Size(13, 13);
            this.label60.TabIndex = 63;
            this.label60.Text = "2";
            // 
            // tarantulaSpeedBar
            // 
            this.tarantulaSpeedBar.Location = new System.Drawing.Point(431, 352);
            this.tarantulaSpeedBar.Maximum = 8;
            this.tarantulaSpeedBar.Name = "tarantulaSpeedBar";
            this.tarantulaSpeedBar.Size = new System.Drawing.Size(374, 45);
            this.tarantulaSpeedBar.TabIndex = 62;
            this.tarantulaSpeedBar.Value = 4;
            this.tarantulaSpeedBar.Scroll += new System.EventHandler(this.tarantulaSpeedBar_Scroll);
            // 
            // label61
            // 
            this.label61.AutoSize = true;
            this.label61.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label61.Location = new System.Drawing.Point(932, 35);
            this.label61.Name = "label61";
            this.label61.Size = new System.Drawing.Size(190, 31);
            this.label61.TabIndex = 83;
            this.label61.Text = "Player Health";
            // 
            // label62
            // 
            this.label62.AutoSize = true;
            this.label62.Location = new System.Drawing.Point(1190, 117);
            this.label62.Name = "label62";
            this.label62.Size = new System.Drawing.Size(19, 13);
            this.label62.TabIndex = 82;
            this.label62.Text = "18";
            // 
            // label63
            // 
            this.label63.AutoSize = true;
            this.label63.Location = new System.Drawing.Point(1147, 117);
            this.label63.Name = "label63";
            this.label63.Size = new System.Drawing.Size(19, 13);
            this.label63.TabIndex = 81;
            this.label63.Text = "16";
            // 
            // label64
            // 
            this.label64.AutoSize = true;
            this.label64.Location = new System.Drawing.Point(1103, 117);
            this.label64.Name = "label64";
            this.label64.Size = new System.Drawing.Size(19, 13);
            this.label64.TabIndex = 80;
            this.label64.Text = "14";
            // 
            // label65
            // 
            this.label65.AutoSize = true;
            this.label65.Location = new System.Drawing.Point(1062, 117);
            this.label65.Name = "label65";
            this.label65.Size = new System.Drawing.Size(19, 13);
            this.label65.TabIndex = 79;
            this.label65.Text = "12";
            // 
            // label66
            // 
            this.label66.AutoSize = true;
            this.label66.Location = new System.Drawing.Point(1017, 117);
            this.label66.Name = "label66";
            this.label66.Size = new System.Drawing.Size(19, 13);
            this.label66.TabIndex = 78;
            this.label66.Text = "10";
            // 
            // label67
            // 
            this.label67.AutoSize = true;
            this.label67.Location = new System.Drawing.Point(975, 117);
            this.label67.Name = "label67";
            this.label67.Size = new System.Drawing.Size(13, 13);
            this.label67.TabIndex = 77;
            this.label67.Text = "8";
            // 
            // label68
            // 
            this.label68.AutoSize = true;
            this.label68.Location = new System.Drawing.Point(931, 117);
            this.label68.Name = "label68";
            this.label68.Size = new System.Drawing.Size(13, 13);
            this.label68.TabIndex = 76;
            this.label68.Text = "6";
            // 
            // label69
            // 
            this.label69.AutoSize = true;
            this.label69.Location = new System.Drawing.Point(889, 117);
            this.label69.Name = "label69";
            this.label69.Size = new System.Drawing.Size(13, 13);
            this.label69.TabIndex = 75;
            this.label69.Text = "4";
            // 
            // label70
            // 
            this.label70.AutoSize = true;
            this.label70.Location = new System.Drawing.Point(847, 117);
            this.label70.Name = "label70";
            this.label70.Size = new System.Drawing.Size(13, 13);
            this.label70.TabIndex = 74;
            this.label70.Text = "2";
            // 
            // playerHealthBar
            // 
            this.playerHealthBar.Location = new System.Drawing.Point(839, 69);
            this.playerHealthBar.Maximum = 8;
            this.playerHealthBar.Name = "playerHealthBar";
            this.playerHealthBar.Size = new System.Drawing.Size(374, 45);
            this.playerHealthBar.TabIndex = 73;
            this.playerHealthBar.Value = 4;
            this.playerHealthBar.Scroll += new System.EventHandler(this.playerHealthBar_Scroll);
            // 
            // label71
            // 
            this.label71.AutoSize = true;
            this.label71.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label71.Location = new System.Drawing.Point(928, 175);
            this.label71.Name = "label71";
            this.label71.Size = new System.Drawing.Size(210, 31);
            this.label71.TabIndex = 94;
            this.label71.Text = "Player Bubbles";
            // 
            // label72
            // 
            this.label72.AutoSize = true;
            this.label72.Location = new System.Drawing.Point(1188, 257);
            this.label72.Name = "label72";
            this.label72.Size = new System.Drawing.Size(25, 13);
            this.label72.TabIndex = 93;
            this.label72.Text = "900";
            // 
            // label73
            // 
            this.label73.AutoSize = true;
            this.label73.Location = new System.Drawing.Point(1144, 257);
            this.label73.Name = "label73";
            this.label73.Size = new System.Drawing.Size(25, 13);
            this.label73.TabIndex = 92;
            this.label73.Text = "800";
            // 
            // label74
            // 
            this.label74.AutoSize = true;
            this.label74.Location = new System.Drawing.Point(1099, 257);
            this.label74.Name = "label74";
            this.label74.Size = new System.Drawing.Size(25, 13);
            this.label74.TabIndex = 91;
            this.label74.Text = "700";
            // 
            // label75
            // 
            this.label75.AutoSize = true;
            this.label75.Location = new System.Drawing.Point(1059, 257);
            this.label75.Name = "label75";
            this.label75.Size = new System.Drawing.Size(25, 13);
            this.label75.TabIndex = 90;
            this.label75.Text = "600";
            // 
            // label76
            // 
            this.label76.AutoSize = true;
            this.label76.Location = new System.Drawing.Point(1015, 257);
            this.label76.Name = "label76";
            this.label76.Size = new System.Drawing.Size(25, 13);
            this.label76.TabIndex = 89;
            this.label76.Text = "500";
            // 
            // label77
            // 
            this.label77.AutoSize = true;
            this.label77.Location = new System.Drawing.Point(975, 257);
            this.label77.Name = "label77";
            this.label77.Size = new System.Drawing.Size(25, 13);
            this.label77.TabIndex = 88;
            this.label77.Text = "400";
            // 
            // label78
            // 
            this.label78.AutoSize = true;
            this.label78.Location = new System.Drawing.Point(931, 257);
            this.label78.Name = "label78";
            this.label78.Size = new System.Drawing.Size(25, 13);
            this.label78.TabIndex = 87;
            this.label78.Text = "300";
            // 
            // label79
            // 
            this.label79.AutoSize = true;
            this.label79.Location = new System.Drawing.Point(889, 257);
            this.label79.Name = "label79";
            this.label79.Size = new System.Drawing.Size(25, 13);
            this.label79.TabIndex = 86;
            this.label79.Text = "200";
            // 
            // label80
            // 
            this.label80.AutoSize = true;
            this.label80.Location = new System.Drawing.Point(846, 257);
            this.label80.Name = "label80";
            this.label80.Size = new System.Drawing.Size(25, 13);
            this.label80.TabIndex = 85;
            this.label80.Text = "100";
            // 
            // playerBubblesBar
            // 
            this.playerBubblesBar.Location = new System.Drawing.Point(839, 209);
            this.playerBubblesBar.Maximum = 8;
            this.playerBubblesBar.Name = "playerBubblesBar";
            this.playerBubblesBar.Size = new System.Drawing.Size(374, 45);
            this.playerBubblesBar.TabIndex = 84;
            this.playerBubblesBar.Value = 4;
            this.playerBubblesBar.Scroll += new System.EventHandler(this.playerBubblesBar_Scroll);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1235, 586);
            this.Controls.Add(this.label71);
            this.Controls.Add(this.label72);
            this.Controls.Add(this.label73);
            this.Controls.Add(this.label74);
            this.Controls.Add(this.label75);
            this.Controls.Add(this.label76);
            this.Controls.Add(this.label77);
            this.Controls.Add(this.label78);
            this.Controls.Add(this.label79);
            this.Controls.Add(this.label80);
            this.Controls.Add(this.playerBubblesBar);
            this.Controls.Add(this.label61);
            this.Controls.Add(this.label62);
            this.Controls.Add(this.label63);
            this.Controls.Add(this.label64);
            this.Controls.Add(this.label65);
            this.Controls.Add(this.label66);
            this.Controls.Add(this.label67);
            this.Controls.Add(this.label68);
            this.Controls.Add(this.label69);
            this.Controls.Add(this.label70);
            this.Controls.Add(this.playerHealthBar);
            this.Controls.Add(this.label51);
            this.Controls.Add(this.label52);
            this.Controls.Add(this.label53);
            this.Controls.Add(this.label54);
            this.Controls.Add(this.label55);
            this.Controls.Add(this.label56);
            this.Controls.Add(this.label57);
            this.Controls.Add(this.label58);
            this.Controls.Add(this.label59);
            this.Controls.Add(this.label60);
            this.Controls.Add(this.tarantulaSpeedBar);
            this.Controls.Add(this.label41);
            this.Controls.Add(this.label42);
            this.Controls.Add(this.label43);
            this.Controls.Add(this.label44);
            this.Controls.Add(this.label45);
            this.Controls.Add(this.label46);
            this.Controls.Add(this.label47);
            this.Controls.Add(this.label48);
            this.Controls.Add(this.label49);
            this.Controls.Add(this.label50);
            this.Controls.Add(this.flySpeedBar);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.label24);
            this.Controls.Add(this.label34);
            this.Controls.Add(this.label35);
            this.Controls.Add(this.label36);
            this.Controls.Add(this.label37);
            this.Controls.Add(this.label38);
            this.Controls.Add(this.label39);
            this.Controls.Add(this.label40);
            this.Controls.Add(this.antSpeedBar);
            this.Controls.Add(this.saveButton);
            this.Controls.Add(this.quitButton);
            this.Controls.Add(this.label23);
            this.Controls.Add(this.label25);
            this.Controls.Add(this.label26);
            this.Controls.Add(this.label27);
            this.Controls.Add(this.label28);
            this.Controls.Add(this.label29);
            this.Controls.Add(this.label30);
            this.Controls.Add(this.label31);
            this.Controls.Add(this.label32);
            this.Controls.Add(this.label33);
            this.Controls.Add(this.tarantulaHealthBar);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.label14);
            this.Controls.Add(this.label15);
            this.Controls.Add(this.label16);
            this.Controls.Add(this.label17);
            this.Controls.Add(this.label18);
            this.Controls.Add(this.label19);
            this.Controls.Add(this.label20);
            this.Controls.Add(this.label21);
            this.Controls.Add(this.label22);
            this.Controls.Add(this.flyHealthBar);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.antHealthBar);
            this.Name = "Form1";
            this.Text = "saveButton";
            ((System.ComponentModel.ISupportInitialize)(this.antHealthBar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.flyHealthBar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tarantulaHealthBar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.antSpeedBar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.flySpeedBar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tarantulaSpeedBar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.playerHealthBar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.playerBubblesBar)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TrackBar antHealthBar;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.TrackBar flyHealthBar;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.TrackBar tarantulaHealthBar;
        private System.Windows.Forms.Button quitButton;
        private System.Windows.Forms.Button saveButton;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.Label label35;
        private System.Windows.Forms.Label label36;
        private System.Windows.Forms.Label label37;
        private System.Windows.Forms.Label label38;
        private System.Windows.Forms.Label label39;
        private System.Windows.Forms.Label label40;
        private System.Windows.Forms.TrackBar antSpeedBar;
        private System.Windows.Forms.Label label41;
        private System.Windows.Forms.Label label42;
        private System.Windows.Forms.Label label43;
        private System.Windows.Forms.Label label44;
        private System.Windows.Forms.Label label45;
        private System.Windows.Forms.Label label46;
        private System.Windows.Forms.Label label47;
        private System.Windows.Forms.Label label48;
        private System.Windows.Forms.Label label49;
        private System.Windows.Forms.Label label50;
        private System.Windows.Forms.TrackBar flySpeedBar;
        private System.Windows.Forms.Label label51;
        private System.Windows.Forms.Label label52;
        private System.Windows.Forms.Label label53;
        private System.Windows.Forms.Label label54;
        private System.Windows.Forms.Label label55;
        private System.Windows.Forms.Label label56;
        private System.Windows.Forms.Label label57;
        private System.Windows.Forms.Label label58;
        private System.Windows.Forms.Label label59;
        private System.Windows.Forms.Label label60;
        private System.Windows.Forms.TrackBar tarantulaSpeedBar;
        private System.Windows.Forms.Label label61;
        private System.Windows.Forms.Label label62;
        private System.Windows.Forms.Label label63;
        private System.Windows.Forms.Label label64;
        private System.Windows.Forms.Label label65;
        private System.Windows.Forms.Label label66;
        private System.Windows.Forms.Label label67;
        private System.Windows.Forms.Label label68;
        private System.Windows.Forms.Label label69;
        private System.Windows.Forms.Label label70;
        private System.Windows.Forms.TrackBar playerHealthBar;
        private System.Windows.Forms.Label label71;
        private System.Windows.Forms.Label label72;
        private System.Windows.Forms.Label label73;
        private System.Windows.Forms.Label label74;
        private System.Windows.Forms.Label label75;
        private System.Windows.Forms.Label label76;
        private System.Windows.Forms.Label label77;
        private System.Windows.Forms.Label label78;
        private System.Windows.Forms.Label label79;
        private System.Windows.Forms.Label label80;
        private System.Windows.Forms.TrackBar playerBubblesBar;
    }
}

